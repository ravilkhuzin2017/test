<?

require_once ('crest.php');

$razreshenniye_simvoli = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
$random_number = substr(str_shuffle($razreshenniye_simvoli), 0, 15);

function CallAPI($method, $url, $data = false)
{
    $curl = curl_init();
    $clientId = "marat12";
    $clientSecret = "1234";

    $authHeader = base64_encode("$clientId:$clientSecret");

    switch ($method) {
        case "POST":
            curl_setopt($curl, CURLOPT_POST, 1);

            if ($data)
                curl_setopt($curl, CURLOPT_POSTFIELDS, json_encode($data, JSON_UNESCAPED_UNICODE));
            break;
        case "PUT":
            curl_setopt($curl, CURLOPT_PUT, 1);
            break;
        default:
            if ($data)
                $url = sprintf("%s?%s", $url, http_build_query($data));
    }

    curl_setopt(
        $curl,
        CURLOPT_HTTPHEADER,
        array(
            'Content-Type: application/json',
            'Content-Length: ' . strlen(json_encode($data, JSON_UNESCAPED_UNICODE)),
        )
    );

    curl_setopt($curl, CURLOPT_URL, $url);
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);

    $result = curl_exec($curl);

    curl_close($curl);

    return $result;
}

if ($_POST['tag']) {
    $data = $_POST["data"];
    $tag = $_POST["tag"];
    $user = $_POST["user"];
    $fileId = $_POST["fileId"];

    if ($tag == "certificate") {
        if (in_array($data['type'], [5, 9])) {
            function getTypeName($type)
            {
                $typesArr = array("1" => "Справка 2-ндфл", "2" => "Копия трудовой книжки", "5" => "О начисленных страховых взносах", "9" => "О неполучении пособия", "10" => "О среднем заработке для ЦЗН", "12" => "С места работы");
                $res = $typesArr[$type];
                if (!$res)
                    return "Ошибка";
                return $res;
            }

            function getObtainingName($obtainig)
            {
                $obtainingArr = array("2" => "По почте на домашний адрес", "1" => "Лично в офисе");
                $res = $obtainingArr[$obtainig];
                if (!$res)
                    return "Ошибка";
                return $res;
            }


            $paramsAddNewElem = [
                "IBLOCK_TYPE_ID" => "bitrix_processes",
                "IBLOCK_ID" => 63,
                "ELEMENT_CODE" => $random_number,
                "FIELDS" => [
                    "NAME" => "Получение справки",
                    "PROPERTY_273" => getTypeName($data['type']),
                    "PROPERTY_274" => $data['years'],
                    "PROPERTY_275" => getObtainingName($data['obtaining']),
                    "PROPERTY_276" => $data['copies'],
                    "PROPERTY_277" => $data['homeAddress'],
                    "PROPERTY_278" => $data['email'],
                    "PROPERTY_279" => $user,
                ]
            ];

            $resultAddNewElem = CRest::call('lists.element.add', $paramsAddNewElem);

            print_r($resultAddNewElem['result']);
        } else {
            $paramsAddNewElem = [
                "IBLOCK_TYPE_ID" => "lists",
                "IBLOCK_ID" => 59,
                "ELEMENT_CODE" => $random_number,
                "FIELDS" => [
                    "NAME" => "Получение справки",
                    "PROPERTY_256" => $data['type'],
                    "PROPERTY_257" => $user,
                    "PROPERTY_258" => "0",
                    "PROPERTY_259" => "-",
                    "PROPERTY_264" => $data['obtaining'],
                    "PROPERTY_272" => $data['created']
                ]
            ];

            $resultAddNewElem = CRest::call('lists.element.add', $paramsAddNewElem);

            $tmpArr = array("btxuid" => $resultAddNewElem['result'] . "_59");

            $mergedArr = array_merge($data, $tmpArr);

            print_r($mergedArr);

            echo CallAPI("POST", "http://esb.abgarant.ru:9090/applications/DO-btx-dev/api/jsondata/post", $mergedArr);
        }

        exit;
    }

    if ($tag == "docs") {
        $elemName = "Кадровый документ";

        if ($data["docID"] == 0) {
            $elemName = "Детский день";
        }

        $paramsAddNewElem = [
            "IBLOCK_TYPE_ID" => "lists",
            "IBLOCK_ID" => 58,
            "ELEMENT_CODE" => $random_number,
            "FIELDS" => [
                "NAME" => $elemName,
                "PROPERTY_253" => $data['uid'],
                "PROPERTY_255" => "Заявка создана. Ожидает подписания.",
            ]
        ];
        $resultAddNewElem = CRest::call('lists.element.add', $paramsAddNewElem);

        $tmpArr = array("btxuid" => $resultAddNewElem['result'] . "_58");

        $mergedArr = array_merge($data, $tmpArr);

        print_r($mergedArr);

        print_r(CallAPI("POST", "http://esb.abgarant.ru:9090/applications/DO-btx-dev/api/jsondata/post", $mergedArr));

        exit;
    }

    if ($tag == "gf") {
        $resGetFile = CallAPI("POST", "http://esb.abgarant.ru:9090/applications/esbREST-dev/api/todo/post", $fileId);
        print_r($resGetFile);

        // [$element, $iblock] = explode("_", $fileId);

        // $paramsGetElem = [
        //     "IBLOCK_TYPE_ID" => 'lists',
        //     "IBLOCK_ID" => 59,
        //     "ELEMENT_ID" => intval($element)
        // ];

        // $resultGetElem = CRest::call('lists.element.get', $paramsGetElem)['result'][0];

        // $paramsElemUpdate = [
        //     "IBLOCK_TYPE_ID" => 'lists',
        //     "IBLOCK_ID" => 59,
        //     "ELEMENT_ID" => intval($element),
        //     "FIELDS" => [
        //         "NAME" => $resultGetElem['NAME'],
        //         "PROPERTY_256" => $resultGetElem['PROPERTY_256'],
        //         "PROPERTY_257" => $resultGetElem['PROPERTY_257'],
        //         "PROPERTY_258" => $resultGetElem['PROPERTY_258'],
        //         "PROPERTY_259" => $resultGetElem['PROPERTY_259'],
        //         "PROPERTY_264" => $resultGetElem['PROPERTY_264'],
        //         "PROPERTY_272" => date('d.m.Y h:i:s a', time()),
        //         "PROPERTY_284" => $resultGetElem['PROPERTY_284'],
        //         "PROPERTY_296" => 1,
        //     ]
        // ];

        // CRest::call('lists.element.update', $paramsElemUpdate);
        exit;
    }

    if ($tag == "info") {
        $dataArr = [
            'btxuid' => $data['ocguid'],
            'message' => $data['message'],
            'action' => "2"
        ];

        echo CallAPI("POST", "http://esb.abgarant.ru:9090/applications/DO-btx-dev/api/jsondata/post", $dataArr);

        [$element, $iblock] = explode("_", $data['ocguid']);

        $paramsGetElem = [
            "IBLOCK_TYPE_ID" => 'lists',
            "IBLOCK_ID" => 59,
            "ELEMENT_ID" => intval($element)
        ];

        $resultGetElem = CRest::call('lists.element.get', $paramsGetElem)['result'][0];

        $paramsElemUpdate = [
            "IBLOCK_TYPE_ID" => 'lists',
            "IBLOCK_ID" => 59,
            "ELEMENT_ID" => intval($element),
            "FIELDS" => [
                "NAME" => $resultGetElem['NAME'],
                "PROPERTY_256" => $resultGetElem['PROPERTY_256'],
                "PROPERTY_257" => $resultGetElem['PROPERTY_257'],
                "PROPERTY_258" => 1,
                "PROPERTY_259" => $resultGetElem['PROPERTY_259'],
                "PROPERTY_264" => $resultGetElem['PROPERTY_264'],
                "PROPERTY_272" => date('d.m.Y h:i:s a', time()),
                "PROPERTY_284" => $resultGetElem['PROPERTY_284'],
                "PROPERTY_296" => $resultGetElem['PROPERTY_296'],
            ]
        ];

        CRest::call('lists.element.update', $paramsElemUpdate);
        exit;
    }

    if ($tag == "close") {
        $dataArr = [
            'btxuid' => $data['ocguid'],
            'action' => "1"
        ];

        echo CallAPI("POST", "http://esb.abgarant.ru:9090/applications/DO-btx-dev/api/jsondata/post", $dataArr);

        [$element, $iblock] = explode("_", $data['ocguid']);

        $paramsGetElem = [
            "IBLOCK_TYPE_ID" => 'lists',
            "IBLOCK_ID" => 59,
            "ELEMENT_ID" => intval($element)
        ];

        $resultGetElem = CRest::call('lists.element.get', $paramsGetElem)['result'][0];

        $paramsElemUpdate = [
            "IBLOCK_TYPE_ID" => 'lists',
            "IBLOCK_ID" => 59,
            "ELEMENT_ID" => intval($element),
            "FIELDS" => [
                "NAME" => $resultGetElem['NAME'],
                "PROPERTY_256" => $resultGetElem['PROPERTY_256'],
                "PROPERTY_257" => $resultGetElem['PROPERTY_257'],
                "PROPERTY_258" => 4,
                "PROPERTY_259" => $resultGetElem['PROPERTY_259'],
                "PROPERTY_264" => $resultGetElem['PROPERTY_264'],
                "PROPERTY_272" => date('d.m.Y h:i:s a', time()),
                "PROPERTY_284" => $resultGetElem['PROPERTY_284'],
                "PROPERTY_296" => $resultGetElem['PROPERTY_296'],
            ]
        ];

        CRest::call('lists.element.update', $paramsElemUpdate);
        exit;
    }
    if ($tag == 'mark') {
        $dataArr = [
            'btxuid' => $data['btxuid'],
            'email' => "sayfullin@abgarant.ru",
            //$data['user'],
            'evaluationEmployees' => $data['EvaluationEmployees'],
            'commentEvaluationEmployees' => $data['CommentEvaluationEmployees'],
            'evaluationService' => $data['EvaluationService'],
            'commentEvaluationService' => $data['CommentEvaluationService'],
            'dateTime' => date('Ymd H:i:s')
        ];

        print_r($dataArr);

        echo CallAPI("POST", "http://esb.abgarant.ru:9090/applications/DO-btx-dev/api/jsondata/post", $dataArr);

        [$element, $iblock] = explode("_", $data['btxuid']);

        $paramsGetElem = [
            "IBLOCK_TYPE_ID" => 'lists',
            "IBLOCK_ID" => 59,
            "ELEMENT_ID" => intval($element)
        ];

        $resultGetElem = CRest::call('lists.element.get', $paramsGetElem)['result'][0];

        $paramsElemUpdate = [
            "IBLOCK_TYPE_ID" => 'lists',
            "IBLOCK_ID" => 59,
            "ELEMENT_ID" => intval($element),
            "FIELDS" => [
                "NAME" => $resultGetElem['NAME'],
                "PROPERTY_256" => $resultGetElem['PROPERTY_256'],
                "PROPERTY_257" => $resultGetElem['PROPERTY_257'],
                "PROPERTY_258" => 5,
                "PROPERTY_259" => $resultGetElem['PROPERTY_259'],
                "PROPERTY_264" => $resultGetElem['PROPERTY_264'],
                "PROPERTY_272" => date('d.m.Y h:i:s', time()),
                "PROPERTY_284" => $resultGetElem['PROPERTY_284'],
                "PROPERTY_296" => $resultGetElem['PROPERTY_296'],
            ]
        ];

        CRest::call('lists.element.update', $paramsElemUpdate);
        exit;
    }
    if ($tag == 'closeLnaVnd') {
        $dataArr = [
            'ocguid' => $data['ocguid'],
            'action' => "Closelnavnd",
            'closingDate' => date('Ymd H:i:s', time()),
        ];

        print_r($dataArr);

        echo CallAPI("POST", "http://esb.abgarant.ru:9090/applications/DO-btx-dev/api/jsondata/post", $dataArr);

        $paramElem = [
            "IBLOCK_TYPE_ID" => 'lists',
            "IBLOCK_ID" => 71,
            "FILTER" => [
                "PROPERTY_310" => $data['ocguid']
            ]
        ];

        $resultGetElem = CRest::call('lists.element.get', $paramElem)['result'][0];

        $paramsElemUpdate = [
            "IBLOCK_TYPE_ID" => 'lists',
            "IBLOCK_ID" => 71,
            "ELEMENT_ID" => intval($resultGetElem['ID']),
            "FIELDS" => [
                "NAME" => $resultGetElem['NAME'],
                "PROPERTY_309" => $resultGetElem['PROPERTY_309'],
                "PROPERTY_310" => $resultGetElem['PROPERTY_310'],
                "PROPERTY_311" => $resultGetElem['PROPERTY_311'],
                "PROPERTY_312" => $resultGetElem['PROPERTY_312'],
                "PROPERTY_313" => $resultGetElem['PROPERTY_313'],
                "PROPERTY_314" => $resultGetElem['PROPERTY_314'],
                "PROPERTY_315" => $resultGetElem['PROPERTY_315'],
                "PROPERTY_316" => $resultGetElem['PROPERTY_316'],
                "PROPERTY_317" => $resultGetElem['PROPERTY_317'],
                "PROPERTY_318" => "lnavnd5",
                "PROPERTY_319" => $resultGetElem['PROPERTY_319'],
                "PROPERTY_320" => $resultGetElem['PROPERTY_320'],
                "PROPERTY_321" => $resultGetElem['PROPERTY_321'],
                "PROPERTY_395" => $resultGetElem['PROPERTY_395'],
            ]
        ];

        CRest::call('lists.element.update', $paramsElemUpdate);
        exit;
    }
    if ($tag == "requestDSS") {
        $element = $data['elemid'];

        $dataArr = [
            'ocguid' => $data['ocguid'],
            'fileid' => $data['fileid'],
            'btxid' => $data['btxid'],
            'type' => "unep",
            'status' => "gotosign"
        ];

        print_r($dataArr);

        echo CallAPI("POST", "http://esb.abgarant.ru:9090/applications/DO-btx-dev/api/jsondata/post", $dataArr);

        $paramsGetFile = [
            "IBLOCK_TYPE_ID" => 'lists',
            "IBLOCK_ID" => 82,
            "ELEMENT_ID" => intval($element)
        ];

        $resultGetFile = CRest::call('lists.element.get', $paramsGetFile)['result'][0];

        $paramsUpdateFileStatus = [
            "IBLOCK_TYPE_ID" => 'lists',
            "IBLOCK_ID" => 82,
            "ELEMENT_ID" => intval($element),
            "FIELDS" => [
                "NAME" => "UnepWaitDSS",
                "PROPERTY_378" => $resultGetFile['PROPERTY_378'],
                "PROPERTY_379" => $resultGetFile['PROPERTY_379'],
                "PROPERTY_380" => "2",
                "PROPERTY_383" => $resultGetFile['PROPERTY_383'],
            ]
        ];

        CRest::call('lists.element.update', $paramsUpdateFileStatus);
        exit;
    }
    if ($tag == "signDSS") {
        $element = $data['elemid'];

        $paramsGetFile = [
            "IBLOCK_TYPE_ID" => 'lists',
            "IBLOCK_ID" => 82,
            "ELEMENT_ID" => intval($element)
        ];

        $resultGetFile = CRest::call('lists.element.get', $paramsGetFile)['result'][0];

        if (array_key_exists('PROPERTY_383', $resultGetFile)) {

            $dataArr = [
                "refid" => reset($resultGetFile['PROPERTY_383']),
                "value" => $data['value'],
                "ocguid" => $data['ocguid'],
                "btxid" => $data['btxid'],
                "fileid" => $data['fileid'],
                'type' => "unep",
                'status' => "kod"
            ];

            print_r($dataArr);

            echo CallAPI("POST", "http://esb.abgarant.ru:9090/applications/DO-btx-dev/api/jsondata/post", $dataArr);

            $paramsUpdateFileStatus = [
                "IBLOCK_TYPE_ID" => 'lists',
                "IBLOCK_ID" => 82,
                "ELEMENT_ID" => intval($element),
                "FIELDS" => [
                    "NAME" => "UnepWaitDSS",
                    "PROPERTY_378" => $resultGetFile['PROPERTY_378'],
                    "PROPERTY_379" => $resultGetFile['PROPERTY_379'],
                    "PROPERTY_380" => "3",
                    "PROPERTY_383" => $resultGetFile['PROPERTY_383'],
                ]
            ];
            print_r(CRest::call('lists.element.update', $paramsUpdateFileStatus));
            exit;
        } else {
            print_r(json_encode(['message' => "Транзакция не присвоена", 'code' => "-1"]));
            exit;
        }
    }
    if ($tag == "closeUnep") {
        // ЭТО ТЕСТОВЫЙ КОД, НАДО ПЕРЕПИСАТЬ!
        $dataTMP = json_decode($data, true);

        $dataArr = [
            "ocguid" => $dataTMP['ocguid'],
            'action' => "closeunep",
            'closingDate' => date('Ymd H:i:s', time()),
        ];

        print_r($dataArr);

        echo CallAPI("POST", "http://esb.abgarant.ru:9090/applications/DO-btx-dev/api/jsondata/post", $dataArr);
    }

    $statusVocation = 0;
    if ($tag == "closeVocation") {
        $dataArr = [
            // ? тут было $_POST['ID_1C'];
            // "ocguid" => $_POST['ID_1C'],
            "ocguid" => $data["ID_1C"],
            'closingDate' => date('Ymd H:i:s', time()),
            "action" => "closevacationdates",
        ];

        $statusVocation = 2;

        $notifyElement = CIBlockElement::GetByID($data["ID"])->Fetch();

        print_r($notifyElement);

        echo CallAPI("POST", "http://esb.abgarant.ru:9090/applications/DO-btx-dev/api/jsondata/post", $dataArr);
        // print_r($dataArr);
    }

    if ($tag == "closeVocationWithTransfer") {

        $dateFormat = 'Y-m-d';
        $newDateFormat = 'Ymd H:i:s';
        $dateStart = dateFormatFunc($dateFormat, $newDateFormat, $data["dateStart"]);//? было $_POST["dateStart"]
        $dateFinish = dateFormatFunc($dateFormat, $newDateFormat, $data["dateFinish"]); //? было $_POST["dateFinish"]
        // $dateStart = dateFormatFunc($dateFormat,$newDateFormat,$_POST["dateFinish"]);
        // $dateFinish = dateFormatFunc($dateFormat,$newDateFormat,$_POST["dateFinish"]);

        $dataArr = [
            "ocguid" => $data["ID_1C"], //? было $_POST["ID_1C"]
            // "ocguid" => $_POST["ID_1C"],
            'closingDate' => date('Ymd H:i:s', time()),
            "action" => "vacationtransfer",
            "newbegindate" => $dateStart,
            "newanddate" => $dateFinish,
            "message" => $data["message"], //? было $_POST["message"]
            // "message" => $_POST["message"],
        ];
        $statusVocation = 3;
        echo CallAPI("POST", "http://esb.abgarant.ru:9090/applications/DO-btx-dev/api/jsondata/post", $dataArr);
        print_r($dataArr);
    }

    $idBlock = 0;
    $resultGetElem = 0;
    if (($tag == "closeVocation") || ($tag == "closeVocationWithTransfer")) {
        $idBlock = $data['ID_block']; //? Тут было $_POST['ID_block']
        // $idBlock = $_POST['ID_block'];
        $paramsGetElem = [
            "IBLOCK_TYPE_ID" => 'lists',
            "IBLOCK_ID" => 85,
            "ELEMENT_ID" => intval($idBlock),
        ];

        $resultGetElem = CRest::call('lists.element.get', $paramsGetElem)['result'][0];

        $paramsUpdateVocation = [
            "IBLOCK_TYPE_ID" => 'lists',
            "IBLOCK_ID" => 85,
            "ELEMENT_ID" => intval($idBlock),
            "FIELDS" => [
                "NAME" => "VocationDocument",
                "PROPERTY_397" => $resultGetElem["PROPERTY_397"],
                "PROPERTY_412" => $resultGetElem["PROPERTY_412"],
                "PROPERTY_398" => $resultGetElem["PROPERTY_398"],
                "PROPERTY_413" => $resultGetElem["PROPERTY_413"],
                "PROPERTY_405" => $resultGetElem["PROPERTY_405"],
                "PROPERTY_406" => $resultGetElem["PROPERTY_406"],
                "PROPERTY_402" => $resultGetElem["PROPERTY_402"],
                "PROPERTY_403" => $resultGetElem["PROPERTY_403"],
                "PROPERTY_404" => $resultGetElem["PROPERTY_404"],
                "PROPERTY_408" => $resultGetElem["PROPERTY_408"],
                "PROPERTY_399" => $resultGetElem["PROPERTY_399"],
                "PROPERTY_414" => $resultGetElem["PROPERTY_414"],
                "PROPERTY_401" => $resultGetElem["PROPERTY_401"],
                "PROPERTY_409" => $statusVocation,
                "PROPERTY_400" => $resultGetElem["PROPERTY_400"],
                "PROPERTY_453" => $tag == "closeVocationWithTransfer" ? $data["dateStart"] : $resultGetElem["PROPERTY_453"],
                // "PROPERTY_453" => $tag == "closeVocationWithTransfer" ? $_POST["dateStart"] : $resultGetElem["PROPERTY_453"],
                "PROPERTY_454" => $tag == "closeVocationWithTransfer" ? $data["dateFinish"] : $resultGetElem["PROPERTY_454"]
                // "PROPERTY_454" => $tag == "closeVocationWithTransfer" ? $_POST["dateFinish"] : $resultGetElem["PROPERTY_454"]
            ]
        ];
        CRest::call('lists.element.update', $paramsUpdateVocation);
    }

    if ($tag == "closeInfoVacation") {
        $dataArr = [
            "ocguid" => $data['ocguid'],
            "action" => "closeinfovacation",
            'closingDate' => date('Ymd H:i:s', time()),
        ];
        $idElem = $data['idElem'];

        echo CallAPI("POST", "http://esb.abgarant.ru:9090/applications/DO-btx-dev/api/jsondata/post", $dataArr);

        $paramsGetElem = [
            "IBLOCK_TYPE_ID" => 'lists',
            "IBLOCK_ID" => 87,
            "ELEMENT_ID" => intval($idElem),
        ];

        $resultGetElem = CRest::call('lists.element.get', $paramsGetElem)['result'][0];

        $paramsUpdateVocation = [
            "IBLOCK_TYPE_ID" => 'lists',
            "IBLOCK_ID" => 87,
            "ELEMENT_ID" => intval($idElem),
            "FIELDS" => [
                "NAME" => "VocationDocument",
                "PROPERTY_415" => $resultGetElem["PROPERTY_415"],
                "PROPERTY_416" => $resultGetElem["PROPERTY_416"],
                "PROPERTY_417" => $resultGetElem["PROPERTY_417"],
                "PROPERTY_418" => $resultGetElem["PROPERTY_418"],
                "PROPERTY_419" => $resultGetElem["PROPERTY_419"],
                "PROPERTY_420" => $resultGetElem["PROPERTY_420"],
                "PROPERTY_421" => $resultGetElem["PROPERTY_421"],
                "PROPERTY_422" => $resultGetElem["PROPERTY_422"],
                "PROPERTY_423" => $resultGetElem["PROPERTY_423"],
                "PROPERTY_424" => $resultGetElem["PROPERTY_424"],
                "PROPERTY_425" => "infoVoc4",
            ]
        ];
        CRest::call('lists.element.update', $paramsUpdateVocation);

        print_r($dataArr);
    }

    if ($tag == 'markInfoVac') {
        $dataArr = [
            'btxuid' => $data['btxuid'],
            'email' => $data['email'],
            'evaluationEmployees' => $data['EvaluationEmployees'],
            'commentEvaluationEmployees' => $data['CommentEvaluationEmployees'],
            'evaluationService' => $data['EvaluationService'],
            'commentEvaluationService' => $data['CommentEvaluationService'],
            'dateTime' => date('Ymd H:i:s'),
        ];

        print_r($dataArr);

        echo CallAPI("POST", "http://esb.abgarant.ru:9090/applications/DO-btx-dev/api/jsondata/post", $dataArr);

        $idElem = $data['idElem'];

        $paramsGetElem = [
            "IBLOCK_TYPE_ID" => 'lists',
            "IBLOCK_ID" => 87,
            "ELEMENT_ID" => intval($idElem),
        ];

        $resultGetElem = CRest::call('lists.element.get', $paramsGetElem)['result'][0];

        $paramsUpdateVocation = [
            "IBLOCK_TYPE_ID" => 'lists',
            "IBLOCK_ID" => 87,
            "ELEMENT_ID" => intval($idElem),
            "FIELDS" => [
                "NAME" => "VocationDocument",
                "PROPERTY_415" => $resultGetElem["PROPERTY_415"],
                "PROPERTY_416" => $resultGetElem["PROPERTY_416"],
                "PROPERTY_417" => $resultGetElem["PROPERTY_417"],
                "PROPERTY_418" => $resultGetElem["PROPERTY_418"],
                "PROPERTY_419" => $resultGetElem["PROPERTY_419"],
                "PROPERTY_420" => $resultGetElem["PROPERTY_420"],
                "PROPERTY_421" => $resultGetElem["PROPERTY_421"],
                "PROPERTY_422" => $resultGetElem["PROPERTY_422"],
                "PROPERTY_423" => $resultGetElem["PROPERTY_423"],
                "PROPERTY_424" => $resultGetElem["PROPERTY_424"],
                "PROPERTY_425" => "infoVoc5",
            ]
        ];
        CRest::call('lists.element.update', $paramsUpdateVocation);
        exit;
    }

    if ($tag == "closevacationdecision") {
        $dataArr = [
            "ocguid" => $data['ocguid'],
            "action" => "closevacationdecision",
            'closingDate' => date('Ymd H:i:s', time()),
        ];
        $idElem = $data['idElem'];

        echo CallAPI("POST", "http://esb.abgarant.ru:9090/applications/DO-btx-dev/api/jsondata/post", $dataArr);

        $paramsGetElem = [
            "IBLOCK_TYPE_ID" => 'lists',
            "IBLOCK_ID" => 88,
            "ELEMENT_ID" => intval($idElem),
        ];

        $resultGetElem = CRest::call('lists.element.get', $paramsGetElem)['result'][0];

        $paramsUpdateVocation = [
            "IBLOCK_TYPE_ID" => 'lists',
            "IBLOCK_ID" => 88,
            "ELEMENT_ID" => intval($idElem),
            "FIELDS" => [
                "NAME" => "VocationDocument",
                "PROPERTY_428" => $resultGetElem["PROPERTY_428"],
                "PROPERTY_429" => $resultGetElem["PROPERTY_429"],
                "PROPERTY_430" => $resultGetElem["PROPERTY_430"],
                "PROPERTY_431" => $resultGetElem["PROPERTY_431"],
                "PROPERTY_432" => $resultGetElem["PROPERTY_432"],
                "PROPERTY_433" => $resultGetElem["PROPERTY_433"],
                "PROPERTY_434" => $resultGetElem["PROPERTY_434"],
                "PROPERTY_435" => $resultGetElem["PROPERTY_435"],
                "PROPERTY_436" => $resultGetElem["PROPERTY_436"],
                "PROPERTY_437" => $resultGetElem["PROPERTY_437"],
                "PROPERTY_438" => "vocDes5",
                "PROPERTY_439" => $resultGetElem["PROPERTY_439"],
                "PROPERTY_463" => $resultGetElem["PROPERTY_463"],
            ]
        ];
        CRest::call('lists.element.update', $paramsUpdateVocation);

        print_r($dataArr);
    }

    if ($tag == "leaveOnDemand") {

        $paramsAddNewElem = [
            "IBLOCK_TYPE_ID" => "lists",
            "IBLOCK_ID" => 90,
            "ELEMENT_CODE" => $random_number,
            "FIELDS" => [
                "NAME" => "Отпуск по требованию",
                "PROPERTY_443" => $data['email'],
                "PROPERTY_444" => '',
                "PROPERTY_445" => $data['dateStart'],
                "PROPERTY_446" => $data['dateFinish'],
                "PROPERTY_447" => $data['payment'],
                "PROPERTY_448" => $data['message'],
                "PROPERTY_514" => $data['uid'],
                "PROPERTY_515" => "-",
            ]
        ];

        $resultAddNewElem = CRest::call('lists.element.add', $paramsAddNewElem);

        $idElem = $resultAddNewElem['result'];

        $paramsGetElem = [
            "IBLOCK_TYPE_ID" => 'lists',
            "IBLOCK_ID" => 90,
            "ELEMENT_ID" => intval($idElem),
        ];

        $resultGetElem = CRest::call('lists.element.get', $paramsGetElem)['result'][0];

        $paramsUpdate = [
            "IBLOCK_TYPE_ID" => 'lists',
            "IBLOCK_ID" => 90,
            "ELEMENT_ID" => intval($idElem),
            "FIELDS" => [
                "NAME" => "Отпуск по требованию",
                "PROPERTY_443" => $resultGetElem["PROPERTY_443"],
                "PROPERTY_444" => $idElem,
                "PROPERTY_445" => $resultGetElem["PROPERTY_445"],
                "PROPERTY_446" => $resultGetElem["PROPERTY_446"],
                "PROPERTY_447" => $resultGetElem["PROPERTY_447"],
                "PROPERTY_448" => $resultGetElem["PROPERTY_448"],
                "PROPERTY_514" => $resultGetElem["PROPERTY_514"],
                "PROPERTY_515" => $resultGetElem["PROPERTY_515"],
            ]
        ];
        CRest::call('lists.element.update', $paramsUpdate);

        $dateFormat = 'Y-m-d';
        $newDateFormat = 'Ymd H:i:s';
        // $dateStart = dateFormatFunc($dateFormat, $newDateFormat, $_POST["dateStart"]);
        // $dateFinish = dateFormatFunc($dateFormat, $newDateFormat, $_POST["dateFinish"]);
        $dateStart = dateFormatFunc($dateFormat, $newDateFormat, $data["dateStart"]);
        $dateFinish = dateFormatFunc($dateFormat, $newDateFormat, $data["dateFinish"]);

        $dataArr = [
            // "email" => $_POST['email'],
            "email" => $data['email'],
            "dateStart" => $dateStart,
            "dateFinish" => $dateFinish,
            // "payment" => $_POST['payment'],
            // "comment" => $_POST['message'],
            "payment" => $data['payment'],
            "comment" => $data['message'],
            "btxuid" => $idElem,
            "type" => "leaveOnDemand",
        ];

        print_r($dataArr);

        echo CallAPI("POST", "http://esb.abgarant.ru:9090/applications/DO-btx-dev/api/jsondata/post", $dataArr);
    }

    if ($tag == "closeLeaveOnDemand") {

        $dataArr = [
            "ocguid" => $data['ocguid'],
            "action" => "closeLeaveOnDemand",
            'closingDate' => date('Ymd H:i:s', time()),
        ];
        $idElem = $data['idElem'];

        echo CallAPI("POST", "http://esb.abgarant.ru:9090/applications/DO-btx-dev/api/jsondata/post", $dataArr);

        $paramsGetElem = [
            "IBLOCK_TYPE_ID" => 'lists',
            "IBLOCK_ID" => 88,
            "ELEMENT_ID" => intval($idElem),
        ];

        $resultGetElem = CRest::call('lists.element.get', $paramsGetElem)['result'][0];

        $paramsUpdateVocation = [
            "IBLOCK_TYPE_ID" => 'lists',
            "IBLOCK_ID" => 88,
            "ELEMENT_ID" => intval($idElem),
            "FIELDS" => [
                "NAME" => "LeaveOnDemand",
                "PROPERTY_428" => $resultGetElem["PROPERTY_428"],
                "PROPERTY_429" => $resultGetElem["PROPERTY_429"],
                "PROPERTY_430" => $resultGetElem["PROPERTY_430"],
                "PROPERTY_431" => $resultGetElem["PROPERTY_431"],
                "PROPERTY_432" => $resultGetElem["PROPERTY_432"],
                "PROPERTY_433" => $resultGetElem["PROPERTY_433"],
                "PROPERTY_434" => $resultGetElem["PROPERTY_434"],
                "PROPERTY_435" => $resultGetElem["PROPERTY_435"],
                "PROPERTY_436" => $resultGetElem["PROPERTY_436"],
                "PROPERTY_437" => $resultGetElem["PROPERTY_437"],
                "PROPERTY_438" => "leaveDem4",
                "PROPERTY_439" => $resultGetElem["PROPERTY_439"],
                "PROPERTY_463" => $resultGetElem["PROPERTY_463"],
            ]
        ];
        CRest::call('lists.element.update', $paramsUpdateVocation);

        print_r($dataArr);
        exit;
    }

    if ($tag == "markLeaveDem") {

        $dataArr = [
            'btxuid' => $data['btxuid'],
            'email' => $data['email'],
            'evaluationEmployees' => $data['EvaluationEmployees'],
            'commentEvaluationEmployees' => $data['CommentEvaluationEmployees'],
            'evaluationService' => $data['EvaluationService'],
            'commentEvaluationService' => $data['CommentEvaluationService'],
            'dateTime' => date('Ymd H:i:s'),
        ];

        $idElem = $data['idElem'];

        print_r(intval($idElem));

        echo CallAPI("POST", "http://esb.abgarant.ru:9090/applications/DO-btx-dev/api/jsondata/post", $dataArr);

        $paramsGetElem = [
            "IBLOCK_TYPE_ID" => 'lists',
            "IBLOCK_ID" => 88,
            "ELEMENT_ID" => intval($idElem),
        ];

        $resultGetElem = CRest::call('lists.element.get', $paramsGetElem)['result'][0];

        $paramsUpdateVocation = [
            "IBLOCK_TYPE_ID" => 'lists',
            "IBLOCK_ID" => 88,
            "ELEMENT_ID" => intval($idElem),
            "FIELDS" => [
                "NAME" => "LeaveOnDemand",
                "PROPERTY_428" => $resultGetElem["PROPERTY_428"],
                "PROPERTY_429" => $resultGetElem["PROPERTY_429"],
                "PROPERTY_430" => $resultGetElem["PROPERTY_430"],
                "PROPERTY_431" => $resultGetElem["PROPERTY_431"],
                "PROPERTY_432" => $resultGetElem["PROPERTY_432"],
                "PROPERTY_433" => $resultGetElem["PROPERTY_433"],
                "PROPERTY_434" => $resultGetElem["PROPERTY_434"],
                "PROPERTY_435" => $resultGetElem["PROPERTY_435"],
                "PROPERTY_436" => $resultGetElem["PROPERTY_436"],
                "PROPERTY_437" => $resultGetElem["PROPERTY_437"],
                "PROPERTY_438" => "leaveDem5",
                "PROPERTY_439" => $resultGetElem["PROPERTY_439"],
                "PROPERTY_463" => $resultGetElem["PROPERTY_463"],
            ]
        ];
        CRest::call('lists.element.update', $paramsUpdateVocation);
        exit;
    }

    if ($tag == "closeLeaveOnDemandBreak") {

        $dataArr = [
            "ocguid" => $data['ocguid'],
            "action" => "closeLeaveOnDemandBreak",
            'closingDate' => date('Ymd H:i:s', time()),
        ];
        $idElem = $data['idElem'];

        echo CallAPI("POST", "http://esb.abgarant.ru:9090/applications/DO-btx-dev/api/jsondata/post", $dataArr);

        $paramsGetElem = [
            "IBLOCK_TYPE_ID" => 'lists',
            "IBLOCK_ID" => 88,
            "ELEMENT_ID" => intval($idElem),
        ];

        $resultGetElem = CRest::call('lists.element.get', $paramsGetElem)['result'][0];

        $paramsUpdateVocation = [
            "IBLOCK_TYPE_ID" => 'lists',
            "IBLOCK_ID" => 88,
            "ELEMENT_ID" => intval($idElem),
            "FIELDS" => [
                "NAME" => "LeaveOnDemandBreak",
                "PROPERTY_428" => $resultGetElem["PROPERTY_428"],
                "PROPERTY_429" => $resultGetElem["PROPERTY_429"],
                "PROPERTY_430" => $resultGetElem["PROPERTY_430"],
                "PROPERTY_431" => $resultGetElem["PROPERTY_431"],
                "PROPERTY_432" => $resultGetElem["PROPERTY_432"],
                "PROPERTY_433" => $resultGetElem["PROPERTY_433"],
                "PROPERTY_434" => $resultGetElem["PROPERTY_434"],
                "PROPERTY_435" => $resultGetElem["PROPERTY_435"],
                "PROPERTY_436" => $resultGetElem["PROPERTY_436"],
                "PROPERTY_437" => $resultGetElem["PROPERTY_437"],
                "PROPERTY_438" => "leaveDemBreak5",
                "PROPERTY_439" => $resultGetElem["PROPERTY_439"],
                "PROPERTY_463" => $resultGetElem["PROPERTY_463"],
            ]
        ];
        CRest::call('lists.element.update', $paramsUpdateVocation);

        print_r($dataArr);
        exit;
    }

    if ($tag == "transferLeaveOnDemand") {

        $paramsAddNewElem = [
            "IBLOCK_TYPE_ID" => "lists",
            "IBLOCK_ID" => 91,
            "ELEMENT_CODE" => $random_number,
            "FIELDS" => [
                "NAME" => "Отпуск по требованию",
                "PROPERTY_464" => $_POST['email'],
                "PROPERTY_465" => '',
                "PROPERTY_466" => $_POST['dateStartSchedule'],
                "PROPERTY_467" => $_POST['dateFinishSchedule'],
                "PROPERTY_468" => $_POST['dateStartNew'],
                "PROPERTY_469" => $_POST['transferWith'],
                "PROPERTY_470" => $_POST['typeHol'],
                "PROPERTY_471" => $_POST['message'],
                "PROPERTY_472" => $_POST['dateFinishNew'],
                "PROPERTY_477" => date('m/d/Y h:i:s a', time()),
            ]
        ];

        $resultAddNewElem = CRest::call('lists.element.add', $paramsAddNewElem);

        $idElem = $resultAddNewElem['result'];

        $paramsGetElem = [
            "IBLOCK_TYPE_ID" => 'lists',
            "IBLOCK_ID" => 91,
            "ELEMENT_ID" => intval($idElem),
        ];

        $resultGetElem = CRest::call('lists.element.get', $paramsGetElem)['result'][0];

        $paramsUpdate = [
            "IBLOCK_TYPE_ID" => 'lists',
            "IBLOCK_ID" => 91,
            "ELEMENT_ID" => intval($idElem),
            "FIELDS" => [
                "NAME" => "Отпуск по требованию",
                "PROPERTY_464" => $resultGetElem["PROPERTY_464"],
                "PROPERTY_465" => $idElem,
                "PROPERTY_466" => $resultGetElem["PROPERTY_466"],
                "PROPERTY_467" => $resultGetElem["PROPERTY_467"],
                "PROPERTY_468" => $resultGetElem["PROPERTY_468"],
                "PROPERTY_469" => $resultGetElem["PROPERTY_469"],
                "PROPERTY_470" => $resultGetElem["PROPERTY_470"],
                "PROPERTY_471" => $resultGetElem["PROPERTY_471"],
                "PROPERTY_472" => $resultGetElem["PROPERTY_472"],
                "PROPERTY_477" => $resultGetElem["PROPERTY_477"],
            ]
        ];
        CRest::call('lists.element.update', $paramsUpdate);

        $dateFormat = 'Y-m-d';
        $newDateFormat = 'Ymd H:i:s';
        $dateStartNew = dateFormatFunc($dateFormat, $newDateFormat, $_POST["dateStartNew"]);
        $dateFinishNew = dateFormatFunc($dateFormat, $newDateFormat, $_POST["dateFinishNew"]);
        $dateStartSchedule = dateFormatFunc($dateFormat, $newDateFormat, $_POST["dateStartSchedule"]);
        $dateFinishSchedule = dateFormatFunc($dateFormat, $newDateFormat, $_POST["dateFinishSchedule"]);

        $dataArr = [
            "email" => $_POST['email'],
            "dateStartSchedule" => $dateStartNew,
            "dateFinishSchedule" => $dateFinishNew,
            "dateStartNew" => $dateStartNew,
            "dateFinishNew" => $dateFinishNew,
            "transferWith" => $_POST['transferWith'],
            "typeHol" => $_POST['typeHol'],
            "comment" => $_POST['message'],
            "btxuid" => $idElem,
            "type" => "transferLeaveOnDemand",
        ];

        print_r($dataArr);

        echo CallAPI("POST", "http://esb.abgarant.ru:9090/applications/DO-btx-dev/api/jsondata/post", $dataArr);
        exit;
    }
    if ($tag == "RemarkNewVacationDates") {

        $dateFormat = 'Y-m-d';
        $newDateFormat = 'Ymd H:i:s';
        $dateStartNew = dateFormatFunc($dateFormat, $newDateFormat, $_POST["dateStartNew"]);
        $dateFinishNew = dateFormatFunc($dateFormat, $newDateFormat, $_POST["dateFinishNew"]);

        $dataArr = [
            "btxuid" => $_POST['btxuid'],
            "ocguid" => $_POST['ocguid'],
            "dateStartNew" => $dateStartNew,
            "dateFinishNew" => $dateFinishNew,
            "email" => $_POST['email'],
            "action" => "RemarkNewVacationDates",
        ];

        print_r($dataArr);

        echo CallAPI("POST", "http://esb.abgarant.ru:9090/applications/DO-btx-dev/api/jsondata/post", $dataArr);

        $paramsGetElem = [
            "IBLOCK_TYPE_ID" => 'lists',
            "IBLOCK_ID" => 88,
            "FILTER" => ["PROPERTY_428" => $_POST['ocguid']],
        ];

        $resultGetElem = CRest::call('lists.element.get', $paramsGetElem)['result'][0];

        $paramsUpdateVocation = [
            "IBLOCK_TYPE_ID" => 'lists',
            "IBLOCK_ID" => 88,
            "ELEMENT_ID" => $resultGetElem['ID'],
            "FIELDS" => [
                "NAME" => "TransferLeaveOnDemandBreak",
                "PROPERTY_428" => $resultGetElem["PROPERTY_428"],
                "PROPERTY_429" => $resultGetElem["PROPERTY_429"],
                "PROPERTY_430" => $resultGetElem["PROPERTY_430"],
                "PROPERTY_431" => $resultGetElem["PROPERTY_431"],
                "PROPERTY_432" => $resultGetElem["PROPERTY_432"],
                "PROPERTY_433" => $resultGetElem["PROPERTY_433"],
                "PROPERTY_434" => $resultGetElem["PROPERTY_434"],
                "PROPERTY_435" => $resultGetElem["PROPERTY_435"],
                "PROPERTY_436" => $resultGetElem["PROPERTY_436"],
                "PROPERTY_437" => $resultGetElem["PROPERTY_437"],
                "PROPERTY_438" => "transferLeaveDemBreak2",
                "PROPERTY_439" => $resultGetElem["PROPERTY_439"],
                "PROPERTY_463" => $resultGetElem["PROPERTY_463"],
            ]
        ];
        CRest::call('lists.element.update', $paramsUpdateVocation);

        exit;
    }
    if ($tag == "StopTransferLeaveOnDemand") {

        $dataArr = [
            "btxuid" => $_POST['btxuid'],
            "ocguid" => $_POST['ocguid'],
            "message" => $_POST['message'],
            "action" => "StopTransferLeaveOnDemand",
        ];

        print_r($dataArr);

        echo CallAPI("POST", "http://esb.abgarant.ru:9090/applications/DO-btx-dev/api/jsondata/post", $dataArr);

        $paramsGetElem = [
            "IBLOCK_TYPE_ID" => 'lists',
            "IBLOCK_ID" => 88,
            "FILTER" => ["PROPERTY_428" => $_POST['ocguid']],
        ];

        $resultGetElem = CRest::call('lists.element.get', $paramsGetElem)['result'][0];

        $paramsUpdateVocation = [
            "IBLOCK_TYPE_ID" => 'lists',
            "IBLOCK_ID" => 88,
            "ELEMENT_ID" => $resultGetElem['ID'],
            "FIELDS" => [
                "NAME" => "TransferLeaveOnDemandBreak",
                "PROPERTY_428" => $resultGetElem["PROPERTY_428"],
                "PROPERTY_429" => $resultGetElem["PROPERTY_429"],
                "PROPERTY_430" => $resultGetElem["PROPERTY_430"],
                "PROPERTY_431" => $resultGetElem["PROPERTY_431"],
                "PROPERTY_432" => $resultGetElem["PROPERTY_432"],
                "PROPERTY_433" => $resultGetElem["PROPERTY_433"],
                "PROPERTY_434" => $resultGetElem["PROPERTY_434"],
                "PROPERTY_435" => $resultGetElem["PROPERTY_435"],
                "PROPERTY_436" => $resultGetElem["PROPERTY_436"],
                "PROPERTY_437" => $resultGetElem["PROPERTY_437"],
                "PROPERTY_438" => "transferLeaveDemBreak3",
                "PROPERTY_439" => $resultGetElem["PROPERTY_439"],
                "PROPERTY_463" => $resultGetElem["PROPERTY_463"],
            ]
        ];
        CRest::call('lists.element.update', $paramsUpdateVocation);

        exit;
    }

    if ($tag == "closeTransferLeaveOnDemandDecision") {
        $dataArr = [
            "ocguid" => $data['ocguid'],
            "action" => "closeTransferLeaveOnDemandDecision",
            'closingDate' => date('Ymd H:i:s', time()),
        ];
        $idElem = $data['idElem'];

        print_r($dataArr);

        echo CallAPI("POST", "http://esb.abgarant.ru:9090/applications/DO-btx-dev/api/jsondata/post", $dataArr);

        $paramsGetElem = [
            "IBLOCK_TYPE_ID" => 'lists',
            "IBLOCK_ID" => 88,
            "ELEMENT_ID" => intval($idElem),
        ];

        $resultGetElem = CRest::call('lists.element.get', $paramsGetElem)['result'][0];

        $paramsUpdateVocation = [
            "IBLOCK_TYPE_ID" => 'lists',
            "IBLOCK_ID" => 88,
            "ELEMENT_ID" => intval($idElem),
            "FIELDS" => [
                "NAME" => "TransferLeaveOnDemandDecision",
                "PROPERTY_428" => $resultGetElem["PROPERTY_428"],
                "PROPERTY_429" => $resultGetElem["PROPERTY_429"],
                "PROPERTY_430" => $resultGetElem["PROPERTY_430"],
                "PROPERTY_431" => $resultGetElem["PROPERTY_431"],
                "PROPERTY_432" => $resultGetElem["PROPERTY_432"],
                "PROPERTY_433" => $resultGetElem["PROPERTY_433"],
                "PROPERTY_434" => $resultGetElem["PROPERTY_434"],
                "PROPERTY_435" => $resultGetElem["PROPERTY_435"],
                "PROPERTY_436" => $resultGetElem["PROPERTY_436"],
                "PROPERTY_437" => $resultGetElem["PROPERTY_437"],
                "PROPERTY_438" => "tranLeaveOnDemand2",
                "PROPERTY_439" => $resultGetElem["PROPERTY_439"],
                "PROPERTY_463" => $resultGetElem["PROPERTY_463"],
            ]
        ];
        CRest::call('lists.element.update', $paramsUpdateVocation);
        exit;
    }
    if ($tag == "answerstransferLeaveOnDemand") {
        $dataArr = [
            "btxuid" => $_POST['btxuid'],
            "message" => $_POST['message'],
            "action" => "2",
        ];

        print_r($dataArr);

        echo CallAPI("POST", "http://esb.abgarant.ru:9090/applications/DO-btx-dev/api/jsondata/post", $dataArr);

        $params = [
            "IBLOCK_TYPE_ID" => 'lists',
            "IBLOCK_ID" => 92,
            "ELEMENT_ID" => $random_number,
            "FIELDS" => [
                "NAME" => "Ответ",
                "PROPERTY_473" => $_POST['btxuid'],
                "PROPERTY_475" => $_POST['message'],
                "PROPERTY_476" => date('m/d/Y h:i:s a', time()),
            ]
        ];

        $res = CRest::call('lists.element.add', $params);
    }

    if ($tag == "kidsDate") {

        $paramsAddNewElem = [
            "IBLOCK_TYPE_ID" => "lists",
            "IBLOCK_ID" => 95,
            "ELEMENT_CODE" => $random_number,
            "FIELDS" => [
                "NAME" => "Детский день",
                "PROPERTY_478" => $_POST['email'],
                "PROPERTY_479" => $_POST['dateStartChildDay'],
                "PROPERTY_480" => $_POST['startTimeChildDay'],
                "PROPERTY_481" => $_POST['finishTimeChildDay'],
                "PROPERTY_482" => date('m/d/Y h:i:s a', time()),
                "PROPERTY_483" => $_POST['periodTimeChildDay'],
                "PROPERTY_484" => '',
                "PROPERTY_485" => $_POST['message'],
                "PROPERTY_509" => "0",
            ]
        ];

        $resultAddNewElem = CRest::call('lists.element.add', $paramsAddNewElem);

        $idElem = $resultAddNewElem['result'];

        $paramsGetElem = [
            "IBLOCK_TYPE_ID" => 'lists',
            "IBLOCK_ID" => 95,
            "ELEMENT_ID" => intval($idElem),
        ];

        $resultGetElem = CRest::call('lists.element.get', $paramsGetElem)['result'][0];

        $paramsUpdate = [
            "IBLOCK_TYPE_ID" => 'lists',
            "IBLOCK_ID" => 95,
            "ELEMENT_ID" => intval($idElem),
            "FIELDS" => [
                "NAME" => "Детский день",
                "PROPERTY_478" => $resultGetElem["PROPERTY_478"],
                "PROPERTY_479" => $resultGetElem["PROPERTY_479"],
                "PROPERTY_480" => $resultGetElem["PROPERTY_480"],
                "PROPERTY_481" => $resultGetElem["PROPERTY_481"],
                "PROPERTY_482" => $resultGetElem["PROPERTY_482"],
                "PROPERTY_483" => $resultGetElem["PROPERTY_483"],
                "PROPERTY_484" => $idElem,
                "PROPERTY_485" => $resultGetElem["PROPERTY_485"],
                "PROPERTY_509" => $resultGetElem["PROPERTY_509"],
            ]
        ];
        CRest::call('lists.element.update', $paramsUpdate);


        $dataArr = [
            "btxuid" => $idElem,
            'Type' => $_POST['tag'],
            'kidDate' => $_POST['dateStartChildDay'],
            'kidDateStart' => $_POST['startTimeChildDay'],
            'kidDateFinish' => $_POST['finishTimeChildDay'],
            'kidDateHour' => $_POST['periodTimeChildDay'],
            'comment' => $_POST['message'],
            'email' => $_POST['email'],
        ];

        print_r($dataArr);

        echo CallAPI("POST", "http://esb.abgarant.ru:9090/applications/DO-btx-dev/api/jsondata/post", $dataArr);

    }

    if ($tag == "closeKidsDateDecision") {
        $dataArr = [
            "ocguid" => $data['ocguid'],
            "action" => "closeKidsDateDecision",
            'closingDate' => date('Ymd H:i:s', time()),
        ];
        $idElem = $data['idElem'];

        print_r($dataArr);

        echo CallAPI("POST", "http://esb.abgarant.ru:9090/applications/DO-btx-dev/api/jsondata/post", $dataArr);

        $paramsGetElem = [
            "IBLOCK_TYPE_ID" => 'lists',
            "IBLOCK_ID" => 88,
            "ELEMENT_ID" => intval($idElem),
        ];

        $resultGetElem = CRest::call('lists.element.get', $paramsGetElem)['result'][0];

        $paramsUpdateVocation = [
            "IBLOCK_TYPE_ID" => 'lists',
            "IBLOCK_ID" => 88,
            "ELEMENT_ID" => intval($idElem),
            "FIELDS" => [
                "NAME" => "KidsDateDecision",
                "PROPERTY_428" => $resultGetElem["PROPERTY_428"],
                "PROPERTY_429" => $resultGetElem["PROPERTY_429"],
                "PROPERTY_430" => $resultGetElem["PROPERTY_430"],
                "PROPERTY_431" => $resultGetElem["PROPERTY_431"],
                "PROPERTY_432" => $resultGetElem["PROPERTY_432"],
                "PROPERTY_433" => $resultGetElem["PROPERTY_433"],
                "PROPERTY_434" => $resultGetElem["PROPERTY_434"],
                "PROPERTY_435" => $resultGetElem["PROPERTY_435"],
                "PROPERTY_436" => $resultGetElem["PROPERTY_436"],
                "PROPERTY_437" => $resultGetElem["PROPERTY_437"],
                "PROPERTY_438" => "kidsDate5",
                "PROPERTY_439" => $resultGetElem["PROPERTY_439"],
                "PROPERTY_463" => $resultGetElem["PROPERTY_463"],
            ]
        ];
        CRest::call('lists.element.update', $paramsUpdateVocation);
        exit;
    }

    if ($tag == "closeKidsDateResult") {
        $dataArr = [
            "ocguid" => $data['ocguid'],
            "action" => "closeKidsDateResult",
            'closingDate' => date('Ymd H:i:s', time()),
        ];
        $idElem = $data['idElem'];

        print_r($dataArr);

        echo CallAPI("POST", "http://esb.abgarant.ru:9090/applications/DO-btx-dev/api/jsondata/post", $dataArr);

        $paramsGetElem = [
            "IBLOCK_TYPE_ID" => 'lists',
            "IBLOCK_ID" => 88,
            "ELEMENT_ID" => intval($idElem),
        ];

        $resultGetElem = CRest::call('lists.element.get', $paramsGetElem)['result'][0];

        $paramsUpdateVocation = [
            "IBLOCK_TYPE_ID" => 'lists',
            "IBLOCK_ID" => 88,
            "ELEMENT_ID" => intval($idElem),
            "FIELDS" => [
                "NAME" => "KidsDateResult",
                "PROPERTY_428" => $resultGetElem["PROPERTY_428"],
                "PROPERTY_429" => $resultGetElem["PROPERTY_429"],
                "PROPERTY_430" => $resultGetElem["PROPERTY_430"],
                "PROPERTY_431" => $resultGetElem["PROPERTY_431"],
                "PROPERTY_432" => $resultGetElem["PROPERTY_432"],
                "PROPERTY_433" => $resultGetElem["PROPERTY_433"],
                "PROPERTY_434" => $resultGetElem["PROPERTY_434"],
                "PROPERTY_435" => $resultGetElem["PROPERTY_435"],
                "PROPERTY_436" => $resultGetElem["PROPERTY_436"],
                "PROPERTY_437" => $resultGetElem["PROPERTY_437"],
                "PROPERTY_438" => "kidsDateResult5",
                "PROPERTY_439" => $resultGetElem["PROPERTY_439"],
                "PROPERTY_463" => $resultGetElem["PROPERTY_463"],
            ]
        ];
        CRest::call('lists.element.update', $paramsUpdateVocation);
        exit;
    }

    if ($tag == "markKidsDate") {

        $dataArr = [
            'btxuid' => $data['btxuid'],
            'email' => $data['email'],
            'evaluationEmployees' => $data['EvaluationEmployees'],
            'commentEvaluationEmployees' => $data['CommentEvaluationEmployees'],
            'evaluationService' => $data['EvaluationService'],
            'commentEvaluationService' => $data['CommentEvaluationService'],
            'dateTime' => date('Ymd H:i:s'),
        ];

        $idElem = $data['idElem'];

        // print_r(intval($idElem));

        print_r($dataArr);

        echo CallAPI("POST", "http://esb.abgarant.ru:9090/applications/DO-btx-dev/api/jsondata/post", $dataArr);

        $paramsGetElem = [
            "IBLOCK_TYPE_ID" => 'lists',
            "IBLOCK_ID" => 88,
            "ELEMENT_ID" => intval($idElem),
        ];

        $resultGetElem = CRest::call('lists.element.get', $paramsGetElem)['result'][0];

        $paramsUpdateVocation = [
            "IBLOCK_TYPE_ID" => 'lists',
            "IBLOCK_ID" => 88,
            "ELEMENT_ID" => intval($idElem),
            "FIELDS" => [
                "NAME" => "KidsDateResult",
                "PROPERTY_428" => $resultGetElem["PROPERTY_428"],
                "PROPERTY_429" => $resultGetElem["PROPERTY_429"],
                "PROPERTY_430" => $resultGetElem["PROPERTY_430"],
                "PROPERTY_431" => $resultGetElem["PROPERTY_431"],
                "PROPERTY_432" => $resultGetElem["PROPERTY_432"],
                "PROPERTY_433" => $resultGetElem["PROPERTY_433"],
                "PROPERTY_434" => $resultGetElem["PROPERTY_434"],
                "PROPERTY_435" => $resultGetElem["PROPERTY_435"],
                "PROPERTY_436" => $resultGetElem["PROPERTY_436"],
                "PROPERTY_437" => $resultGetElem["PROPERTY_437"],
                "PROPERTY_438" => "kidsDateResult6",
                "PROPERTY_439" => $resultGetElem["PROPERTY_439"],
                "PROPERTY_463" => $resultGetElem["PROPERTY_463"],
            ]
        ];
        CRest::call('lists.element.update', $paramsUpdateVocation);
        exit;
    }

    if ($tag == "closeKidsDateBreak") {
        $dataArr = [
            "ocguid" => $data['ocguid'],
            "action" => "closeKidsDateBreak",
            'closingDate' => date('Ymd H:i:s', time()),
        ];
        $idElem = $data['idElem'];

        print_r($dataArr);

        echo CallAPI("POST", "http://esb.abgarant.ru:9090/applications/DO-btx-dev/api/jsondata/post", $dataArr);

        $paramsGetElem = [
            "IBLOCK_TYPE_ID" => 'lists',
            "IBLOCK_ID" => 88,
            "ELEMENT_ID" => intval($idElem),
        ];

        $resultGetElem = CRest::call('lists.element.get', $paramsGetElem)['result'][0];

        $paramsUpdateVocation = [
            "IBLOCK_TYPE_ID" => 'lists',
            "IBLOCK_ID" => 88,
            "ELEMENT_ID" => intval($idElem),
            "FIELDS" => [
                "NAME" => "kidsDateBreak",
                "PROPERTY_428" => $resultGetElem["PROPERTY_428"],
                "PROPERTY_429" => $resultGetElem["PROPERTY_429"],
                "PROPERTY_430" => $resultGetElem["PROPERTY_430"],
                "PROPERTY_431" => $resultGetElem["PROPERTY_431"],
                "PROPERTY_432" => $resultGetElem["PROPERTY_432"],
                "PROPERTY_433" => $resultGetElem["PROPERTY_433"],
                "PROPERTY_434" => $resultGetElem["PROPERTY_434"],
                "PROPERTY_435" => $resultGetElem["PROPERTY_435"],
                "PROPERTY_436" => $resultGetElem["PROPERTY_436"],
                "PROPERTY_437" => $resultGetElem["PROPERTY_437"],
                "PROPERTY_438" => "kidsDateBreak5",
                "PROPERTY_439" => $resultGetElem["PROPERTY_439"],
                "PROPERTY_463" => $resultGetElem["PROPERTY_463"],
            ]
        ];
        CRest::call('lists.element.update', $paramsUpdateVocation);
        exit;
    }

    if ($tag == "answersKidsDays") {
        $dataArr = [
            "btxuid" => $_POST['btxuid'],
            "message" => $_POST['message'],
            "action" => "2",
        ];

        print_r($dataArr);

        echo CallAPI("POST", "http://esb.abgarant.ru:9090/applications/DO-btx-dev/api/jsondata/post", $dataArr);

        $params = [
            "IBLOCK_TYPE_ID" => 'lists',
            "IBLOCK_ID" => 98,
            "ELEMENT_ID" => $random_number,
            "FIELDS" => [
                "NAME" => "Ответ",
                "PROPERTY_510" => $_POST['btxuid'],
                "PROPERTY_512" => $_POST['message'],
                "PROPERTY_513" => date('m/d/Y h:i:s a', time()),
            ]
        ];

        $res = CRest::call('lists.element.add', $params);
    }

    exit;
}



function dateFormatFunc($dateFormat, $newDateFormat, $dateTime)
{
    $dateTimeResult = DateTime::createFromFormat($dateFormat, $dateTime);
    $newDateString = $dateTimeResult->format($newDateFormat);
    return $newDateString;
}