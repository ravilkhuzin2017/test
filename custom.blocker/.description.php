<?php
$arModuleVersion = [
    "VERSION" => "1.0.0",
    "VERSION_DATE" => "2023-08-22"
];

$module_id = "custom.blocker";

CModule::AddAutoloadClasses(
    $module_id,
    [
        "CustomBlocker" => "include.php",
    ]
);
?>
