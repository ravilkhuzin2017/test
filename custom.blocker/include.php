<?php
class CustomBlocker
{
    public static function onPrologHandler()
    {
        global $USER, $APPLICATION;

        if (!$USER->IsAuthorized()) {
            return;
        }

        $module_id = 'custom.blocker';
        $groupFieldsMap = unserialize(\Bitrix\Main\Config\Option::get($module_id, 'group_fields_map', '')) ?: [];

        $userGroups = $USER->GetUserGroupArray();

        foreach ($groupFieldsMap as $groupID => $data) {
            if (in_array($groupID, $userGroups) && strpos($APPLICATION->GetCurPage(), "/crm/deal/details/") !== false) {
                $fieldsToBlock = explode(',', $data['fields']);
                $blockedStages = explode(',', $data['blocked_stages']);  // Заблокированные стадии

                \Bitrix\Main\Page\Asset::getInstance()->addString("
                    <script>
                        document.addEventListener('DOMContentLoaded', function() {
                            function applyBlockers() {
                                var fields = " . json_encode($fieldsToBlock) . ";
                                fields.forEach(function(fieldId) {
                                    var field = document.querySelector('[data-cid=\"' + fieldId + '\"]');
                                    if (field) {
                                        field.setAttribute('disabled', 'disabled');
                                        field.style.pointerEvents = 'none';
                                    }
                                });

                                var blockedStages = " . json_encode($blockedStages) . ";
                                var stages = document.querySelectorAll('.crm-entity-section-status-step');
                                stages.forEach(function(stage) {
                                    var stageId = stage.getAttribute('data-id');
                                    if (blockedStages.includes(stageId)) {
                                        stage.style.pointerEvents = 'none';
                                        stage.style.opacity = '0.5';
                                    }
                                });
                            }

                            // Применяем блокировки сразу
                            applyBlockers();

                            // Используем MutationObserver для отслеживания изменений в DOM и повторного применения блокировок
                            var observer = new MutationObserver(function(mutations) {
                                mutations.forEach(function(mutation) {
                                    applyBlockers();
                                });
                            });

                            observer.observe(document.body, {
                                childList: true,
                                subtree: true,
                            });
                        });
                    </script>
                ");
            }
        }
    }
}
?>
