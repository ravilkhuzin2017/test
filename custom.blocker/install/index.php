<?php
use Bitrix\Main\ModuleManager;
use Bitrix\Main\EventManager;

class custom_blocker extends CModule
{
    public function __construct()
    {
        $this->MODULE_ID = 'custom.blocker';
        $this->MODULE_NAME = 'Custom Blocker';
        $this->MODULE_DESCRIPTION = 'Модуль для блокировки полей и панели статусов для определённых групп пользователей';
        $this->PARTNER_NAME = 'Эксперт Новострой';
        $this->PARTNER_URI = 'https://expert-novostroy.ru/';
        $this->MODULE_VERSION = '1.0.0';
        $this->MODULE_VERSION_DATE = '30.08.2024';
    }

    public function DoInstall()
    {
        ModuleManager::registerModule($this->MODULE_ID);
        $this->InstallEvents();
    }

    public function DoUninstall()
    {
        $this->UnInstallEvents();
        ModuleManager::unRegisterModule($this->MODULE_ID);
    }

    public function InstallEvents()
    {
        // Регистрация обработчика событий
        EventManager::getInstance()->registerEventHandler(
            'main',
            'OnProlog',
            $this->MODULE_ID,
            'CustomBlocker',
            'onPrologHandler'
        );
    }

    public function UnInstallEvents()
    {
        // Удаление обработчика событий
        EventManager::getInstance()->unRegisterEventHandler(
            'main',
            'OnProlog',
            $this->MODULE_ID,
            'CustomBlocker',
            'onPrologHandler'
        );
    }
}
?>
