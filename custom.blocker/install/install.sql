CREATE TABLE IF NOT EXISTS custom_blocker_settings (
    id INT AUTO_INCREMENT PRIMARY KEY,
    fields_to_block TEXT NOT NULL,
    groups_to_block TEXT NOT NULL,
    block_status_panel BOOLEAN NOT NULL DEFAULT FALSE
);
