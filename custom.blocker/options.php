<?php
use Bitrix\Main\Config\Option;
use Bitrix\Main\Localization\Loc;

Loc::loadMessages(__FILE__);

$module_id = 'custom.blocker';


// Пробуем получить сохраненное значение и десериализовать его
$groupFieldsMap = unserialize(Option::get($module_id, 'group_fields_map', '')) ?: [];

// Обработка удаления группы
if (isset($_GET['delete_group']) && !empty($_GET['delete_group'])) {
    $deleteGroupID = $_GET['delete_group'];
    unset($groupFieldsMap[$deleteGroupID]);
    Option::set($module_id, 'group_fields_map', serialize($groupFieldsMap));
    header("Location: " . $_SERVER['PHP_SELF'] . "?lang=" . LANGUAGE_ID . "&mid=" . $module_id);
    exit;
}

// Обработка POST-запросов
if ($_SERVER['REQUEST_METHOD'] === 'POST' && check_bitrix_sessid()) {
    try {
        if (isset($_POST['group_fields'])) {
            foreach ($_POST['group_fields'] as $groupID => $data) {
                if (!empty($groupID) && !empty($data['fields'])) {
                    $groupFieldsMap[$groupID] = [
                        'fields' => $data['fields'],
                        'blocked_stages' => isset($data['blocked_stages']) ? $data['blocked_stages'] : '',
                    ];
                }
            }
        }

        if (!empty($_POST['new_group']['group']) && !empty($_POST['new_group']['fields'])) {
            $groupID = $_POST['new_group']['group'];
            $newFields = explode(',', $_POST['new_group']['fields']);

            $existingFields = isset($groupFieldsMap[$groupID]['fields']) ? explode(',', $groupFieldsMap[$groupID]['fields']) : [];
            $mergedFields = array_unique(array_merge($existingFields, $newFields));

            $groupFieldsMap[$groupID] = [
                'fields' => implode(',', $mergedFields),
                'blocked_stages' => isset($_POST['new_group']['blocked_stages']) ? $_POST['new_group']['blocked_stages'] : '',
            ];
        }

        Option::set($module_id, 'group_fields_map', serialize($groupFieldsMap));
        echo "<div class='adm-info-message'>Настройки успешно сохранены.</div>";
    } catch (Exception $e) {
        echo "<div class='adm-info-message-wrap adm-info-message-red'>Ошибка при сохранении: " . $e->getMessage() . "</div>";
    }
}
?>

<!-- CSS для стилизации таблицы -->
<style>
    table.internal {
        border-collapse: collapse;
        width: 100%;
        margin-bottom: 20px;
    }
    table.internal th, table.internal td {
        border: 1px solid #ddd;
        padding: 8px;
    }
    table.internal th {
        background-color: #f2f2f2;
        text-align: center;
    }
    table.internal td input[type="text"] {
        width: 95%;
        padding: 5px;
        box-sizing: border-box;
    }
    table.internal td input[type="checkbox"] {
        margin-left: 10px;
    }
    .adm-info-message {
        background-color: #e1f5d1;
        color: #4f8a10;
        border: 1px solid #4f8a10;
        padding: 10px;
        margin: 10px 0;
    }
    .adm-info-message-red {
        background-color: #f8d7da;
        color: #721c24;
        border: 1px solid #f5c6cb;
    }
    .adm-btn {
        background-color: #4CAF50;
        color: white;
        padding: 10px 20px;
        text-align: center;
        text-decoration: none;
        display: inline-block;
        font-size: 14px;
        margin: 10px 0;
        cursor: pointer;
        border-radius: 5px;
        border: none;
    }
    .adm-btn:hover {
        background-color: #45a049;
    }
</style>

<form method="post" action="">
    <?=bitrix_sessid_post();?>

    <h2><?=Loc::getMessage('CUSTOM_BLOCKER_FIELDS_GROUP_TITLE')?></h2>
    <p><?=Loc::getMessage('CUSTOM_BLOCKER_FIELDS_GROUP_DESC')?></p>

    <table cellpadding="5" cellspacing="0" border="1" class="internal">
        <tr class="heading">
            <th><?=Loc::getMessage('CUSTOM_BLOCKER_GROUP_ID')?></th>
            <th><?=Loc::getMessage('CUSTOM_BLOCKER_FIELDS')?></th>
            <th><?=Loc::getMessage('CUSTOM_BLOCKER_BLOCKED_STAGES')?></th>
            <th><?=Loc::getMessage('CUSTOM_BLOCKER_ACTIONS')?></th>
        </tr>
        <?php if ($groupFieldsMap): ?>
            <?php foreach ($groupFieldsMap as $groupID => $data): ?>
            <tr>
                <td><input type="text" name="group_fields[<?=htmlspecialchars($groupID)?>][group]" value="<?=htmlspecialchars($groupID)?>" readonly /></td>
                <td><input type="text" name="group_fields[<?=htmlspecialchars($groupID)?>][fields]" value="<?=htmlspecialchars($data['fields'])?>" /></td>
                <td><input type="text" name="group_fields[<?=htmlspecialchars($groupID)?>][blocked_stages]" value="<?=htmlspecialchars($data['blocked_stages'])?>" placeholder="<?=Loc::getMessage('CUSTOM_BLOCKER_NEW_STAGES')?>" /></td>
                <td>
                    <a href="?lang=<?=LANGUAGE_ID?>&mid=<?=htmlspecialchars($module_id)?>&delete_group=<?=htmlspecialchars($groupID)?>" onclick="return confirm('<?=Loc::getMessage('CUSTOM_BLOCKER_CONFIRM_DELETE')?>');" style="color: red;"><?=Loc::getMessage('CUSTOM_BLOCKER_DELETE')?></a>
                </td>
            </tr>
            <?php endforeach; ?>
        <?php endif; ?>
        <tr>
            <td><input type="text" name="new_group[group]" placeholder="<?=Loc::getMessage('CUSTOM_BLOCKER_NEW_GROUP')?>" /></td>
            <td><input type="text" name="new_group[fields]" placeholder="<?=Loc::getMessage('CUSTOM_BLOCKER_NEW_FIELDS')?>" /></td>
            <td><input type="text" name="new_group[blocked_stages]" placeholder="<?=Loc::getMessage('CUSTOM_BLOCKER_NEW_STAGES')?>" /></td>
            <td></td>
        </tr>
    </table>

    <input type="submit" value="<?=Loc::getMessage('CUSTOM_BLOCKER_SAVE_SETTINGS')?>" class="adm-btn" />
</form>
