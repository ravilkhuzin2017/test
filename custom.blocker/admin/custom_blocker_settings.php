<?php
use Bitrix\Main\Config\Option;
use Bitrix\Main\Loader;
use Bitrix\Main\Localization\Loc;

require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_admin_before.php");

Loc::loadMessages(__FILE__);

$module_id = 'custom.blocker';
Loader::includeModule($module_id);

// Обработка POST-запросов
if ($_SERVER['REQUEST_METHOD'] == 'POST' && check_bitrix_sessid()) {
    Option::set($module_id, 'fields_to_block', $_POST['fields_to_block']);
    Option::set($module_id, 'groups_to_block', $_POST['groups_to_block']);
    Option::set($module_id, 'block_status_panel', $_POST['block_status_panel'] === 'Y' ? 'Y' : 'N');
}

// Получение текущих значений настроек
$fieldsToBlock = Option::get($module_id, 'fields_to_block', '');
$groupsToBlock = Option::get($module_id, 'groups_to_block', '');
$blockStatusPanel = Option::get($module_id, 'block_status_panel', 'N');

// Заголовок страницы
$APPLICATION->SetTitle("Настройки модуля Custom Blocker");

// Подключение административного шаблона
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_admin_after.php");
?>

<form method="post" action="">
    <?=bitrix_sessid_post();?>
    <h2>Настройки блокировки полей</h2>
    <p>Введите ID полей через запятую, которые необходимо заблокировать:</p>
    <input type="text" name="fields_to_block" value="<?=$fieldsToBlock?>" />

    <h2>Настройки блокировки для групп</h2>
    <p>Введите ID групп через запятую, для которых необходимо заблокировать доступ:</p>
    <input type="text" name="groups_to_block" value="<?=$groupsToBlock?>" />

    <h2>Настройки блокировки верхней панели</h2>
    <label>
        <input type="checkbox" name="block_status_panel" value="Y" <?= $blockStatusPanel === 'Y' ? 'checked' : '' ?> />
        Блокировать верхнюю панель
    </label>

    <br><br>
    <input type="submit" value="Сохранить настройки" />
</form>

<?php
// Подключение нижней части административного шаблона
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/epilog_admin.php");
?>
