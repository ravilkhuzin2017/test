<?php
$MESS['CUSTOM_BLOCKER_FIELDS_GROUP_TITLE'] = 'Настройки блокировки полей для групп';
$MESS['CUSTOM_BLOCKER_FIELDS_GROUP_DESC'] = 'Введите ID групп и соответствующие им поля для блокировки. Вы можете добавить несколько групп.';
$MESS['CUSTOM_BLOCKER_GROUP_ID'] = 'ID Группы';
$MESS['CUSTOM_BLOCKER_FIELDS'] = 'ID Полей';
$MESS['CUSTOM_BLOCKER_ACTIONS'] = 'Действия';
$MESS['CUSTOM_BLOCKER_BLOCKED_STAGES'] = 'ID Стадий';
$MESS['CUSTOM_BLOCKER_NEW_GROUP'] = 'ID новой группы';
$MESS['CUSTOM_BLOCKER_NEW_FIELDS'] = 'ID новых полей для блокировки. (ID полей - это data-cid)';
$MESS['CUSTOM_BLOCKER_NEW_STAGES'] = 'Введите ID стадий через запятую. (ID стадий - это data-id)';
$MESS['CUSTOM_BLOCKER_STATUS_BLOCK_TITLE'] = 'Настройки блокировки верхней панели';
$MESS['CUSTOM_BLOCKER_STATUS_BLOCK_DESC'] = 'Блокировать верхнюю панель';
$MESS['CUSTOM_BLOCKER_SAVE_SETTINGS'] = 'Сохранить настройки';
$MESS['CUSTOM_BLOCKER_DELETE'] = 'Удалить';
$MESS['CUSTOM_BLOCKER_CONFIRM_DELETE'] = 'Вы уверены, что хотите удалить эту группу?';
