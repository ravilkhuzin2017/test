<?php
$MESS['CUSTOM_SUPPORT_MODULE_NAME'] = 'Модуль поддержки';
$MESS['CUSTOM_SUPPORT_MODULE_DESCRIPTION'] = 'Модуль техподдержки.';
$MESS['CUSTOM_SUPPORT_PARTNER_NAME'] = 'Ак Барс Гарант';
$MESS['CUSTOM_SUPPORT_PARTNER_URI'] = 'https://abgarant.ru/';
$MESS['CUSTOM_SUPPORT_INSTALL_ERROR'] = 'Ошибка установки модуля';
$MESS['CUSTOM_SUPPORT_UNINSTALL_ERROR'] = 'Ошибка удаления модуля';
$MESS['CUSTOM_SUPPORT_MODULE_NOT_LOADED'] = 'Модуль не загружен';
?>
