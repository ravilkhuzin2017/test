<?php
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php");
require_once $_SERVER['DOCUMENT_ROOT'] . '/bitrix/modules/main/include/prolog_before.php';
use Bitrix\Main\Loader;
use Bitrix\Main\Application;
use Custom\Support\FormsTable;
use Custom\Support\CategoriesTable;
use Bitrix\Socialnetwork\WorkgroupTable;
use Bitrix\Socialnetwork\UserToGroupTable;
use Bitrix\Main\GroupTable;  // Подключаем таблицу групп

if (!Loader::includeModule('custom.support')) {
    echo "Модуль не загружен";
    die();
}

if (!Loader::includeModule('socialnetwork')) {
    echo "Модуль Socialnetwork не загружен";
    die();
}

// Получение реальных проектов из Битрикс
$projects = [];
$rsGroups = WorkgroupTable::getList([
    'select' => ['ID', 'NAME']
]);
while ($arGroup = $rsGroups->fetch()) {
    $projects[$arGroup['ID']] = $arGroup['NAME'];
}

// Получение групп пользователей из Bitrix
$userGroups = [];
$rsUserGroups = GroupTable::getList([
    'select' => ['ID', 'NAME'],
    'filter' => ['ACTIVE' => 'Y']
]);
while ($arUserGroup = $rsUserGroups->fetch()) {
    $userGroups[$arUserGroup['ID']] = $arUserGroup['NAME'];
}

$request = Application::getInstance()->getContext()->getRequest();
$action = $request->get('action');
$errors = [];

// Получение групп текущего пользователя
global $USER;
$currentUserGroups = $USER->GetUserGroupArray();

// Получение проектов, в которых состоит текущий пользователь
$userId = $USER->GetID();
$userProjects = [];
$rsUserProjects = UserToGroupTable::getList([
    'select' => ['GROUP_ID'],
    'filter' => ['=USER_ID' => $userId]
]);
while ($arUserProject = $rsUserProjects->fetch()) {
    $userProjects[] = $arUserProject['GROUP_ID'];
}

// Фильтрация форм по видимости
function filterFormsByVisibility($forms, $currentUserGroups, $userProjects) {
    return array_filter($forms, function($form) use ($currentUserGroups, $userProjects) {
        if ($form['VISIBILITY'] === 'all') {
            return true;
        }
        if ($form['VISIBILITY'] === 'intranet' && in_array(2, $currentUserGroups)) {
            return true;
        }
        if ($form['VISIBILITY'] === 'extranet' && in_array(1, $currentUserGroups)) {
            return true;
        }
        if (in_array($form['VISIBILITY'], $userProjects)) {
            return true;
        }
        if (!empty($form['GROUP_VISIBILITY'])) {
            $groupVisibilities = explode(',', $form['GROUP_VISIBILITY']);
            foreach ($groupVisibilities as $groupId) {
                if (in_array((int)$groupId, $currentUserGroups)) {
                    return true;
                }
            }
        }
        return false;
    });
}

if ($action === 'add' || $action === 'edit') {
    $formId = $request->get('ID');
    $formName = $request->getPost('NAME');
    $categoryId = $request->getPost('CATEGORY_ID');
    $fields = $request->getPost('FIELDS');
    $visibility = $request->getPost('VISIBILITY');
    $groupVisibility = $request->getPost('GROUP_VISIBILITY');
    $businessProcessId = $request->getPost('BUSINESS_PROCESS_ID');
    $processId = $request->getPost('PROCESS_ID'); // Добавляем PROCESS_ID

    if (empty($formName)) {
        $errors[] = 'Название формы не может быть пустым';
    }

    if (empty($errors)) {
        if (is_string($fields)) {
            $fields = json_decode($fields, true);
        }
        $fieldsJson = json_encode($fields, JSON_UNESCAPED_UNICODE);

        if (is_array($groupVisibility)) {
            $groupVisibility = implode(',', $groupVisibility);
        }

        $formData = [
            'NAME' => $formName,
            'CATEGORY_ID' => $categoryId,
            'FIELDS' => $fieldsJson,
            'VISIBILITY' => $visibility,
            'GROUP_VISIBILITY' => $groupVisibility,
            'BUSINESS_PROCESS_ID' => $businessProcessId,
            'PROCESS_ID' => $processId // Новый столбец
        ];

        if ($action === 'add') {
            $result = FormsTable::add($formData);
        } elseif ($action === 'edit' && $formId) {
            $result = FormsTable::update($formId, $formData);
        }

        if (!$result->isSuccess()) {
            $errors = array_merge($errors, $result->getErrorMessages());
        } else {
            LocalRedirect('/support/forms.php');
        }
    }
}

if ($action === 'delete' && $request->get('ID')) {
    $formId = $request->get('ID');
    FormsTable::delete($formId);
    LocalRedirect('/support/forms.php');
}

foreach ($errors as $error) {
    echo '<div class="ui-alert ui-alert-danger">' . $error . '</div>';
}

// Получение форм и фильтрация по видимости
$forms = FormsTable::getList()->fetchAll();
$filteredForms = filterFormsByVisibility($forms, $currentUserGroups, $userProjects);
?>

<!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <title>Формы поддержки</title>
    <style>
        body { font-family: Arial, sans-serif; }
        .container { max-width: 800px; margin: 0 auto; padding: 20px; }
        .form-group { margin-bottom: 15px; }
        .form-group label { display: block; margin-bottom: 5px; }
        .form-group select, .form-group input, .form-group textarea { width: 100%; padding: 8px; box-sizing: border-box; }
        .form-table {
            width: 100%;
            border-collapse: collapse;
            margin-top: 20px;
        }
        .form-table, .form-table th, .form-table td {
            border: 1px solid #ddd;
        }
        .form-table th, .form-table td {
            padding: 10px;
            text-align: left;
        }
        .form-table th {
            background-color: #f4f4f4;
        }
        .edit-mode {
            display: none;
        }
    </style>
    <script>
        document.addEventListener("DOMContentLoaded", function () {
            document.querySelectorAll(".edit-btn").forEach(function (button) {
                button.addEventListener("click", function () {
                    var row = this.closest("tr");
                    row.querySelectorAll(".view-mode").forEach(function (viewElement) {
                        viewElement.style.display = "none";
                    });
                    row.querySelectorAll(".edit-mode").forEach(function (editElement) {
                        editElement.style.display = "block";
                    });
                    this.style.display = "none";
                    row.querySelector(".save-btn").style.display = "inline";
                });
            });

            document.querySelectorAll(".save-btn").forEach(function (button) {
                button.addEventListener("click", function () {
                    var row = this.closest("tr");
                    var formId = row.dataset.formId;
                    var formName = row.querySelector(".form-name-input").value;
                    var categoryId = row.querySelector(".category-id-select").value;
                    var fields = JSON.parse(row.querySelector(".fields-input").value);
                    var visibility = row.querySelector(".visibility-select").value;
                    var groupVisibility = Array.from(row.querySelector(".group-visibility-select").selectedOptions).map(option => option.value).join(',');
                    var businessProcessId = row.querySelector(".business-process-id-input").value;
                    var processId = row.querySelector(".process-id-input").value; // Новый столбец

                    var xhr = new XMLHttpRequest();
                    xhr.open("POST", "/support/forms.php?action=edit&ID=" + formId, true);
                    xhr.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
                    xhr.onreadystatechange = function () {
                        if (xhr.readyState === 4 && xhr.status === 200) {
                            location.reload();
                        }
                    };
                    xhr.send("NAME=" + encodeURIComponent(formName) + "&CATEGORY_ID=" + encodeURIComponent(categoryId) + "&FIELDS=" + encodeURIComponent(JSON.stringify(fields)) + "&VISIBILITY=" + encodeURIComponent(visibility) + "&GROUP_VISIBILITY=" + encodeURIComponent(groupVisibility) + "&BUSINESS_PROCESS_ID=" + encodeURIComponent(businessProcessId) + "&PROCESS_ID=" + encodeURIComponent(processId));
                });
            });

            document.getElementById("add-field-btn").addEventListener("click", function () {
                var container = document.getElementById("fields-container");
                var fieldHtml = '<div class="form-group">' +
                                '<input type="text" name="FIELDS[][name]" placeholder="Название поля" class="form-control" />' +
                                '<select name="FIELDS[][type]" class="form-control">' +
                                '<option value="text">Строка</option>' +
                                '<option value="number">Число</option>' +
                                '<option value="textarea">Текстовое поле</option>' +
                                '<option value="yesno">Да/Нет</option>' +
                                '<option value="date">Дата</option>' +
                                '<option value="datetime">Дата со временем</option>' +
                                '<option value="employee">Привязка к сотруднику</option>' +
                                '<option value="file">Файл</option>' +
                                '</select>' +
                                '</div>';
                container.insertAdjacentHTML('beforeend', fieldHtml);
            });
        });
    </script>
</head>
<body>
    <div class="container">
        <h1>Формы поддержки</h1>

        <form method="post" action="?action=add">
            <div class="form-group">
                <label for="form-name">Название формы</label>
                <input type="text" id="form-name" name="NAME" required>
            </div>
            <div class="form-group">
                <label for="category-id">Категория</label>
                <select id="category-id" name="CATEGORY_ID" required>
                    <?php
                    $categories = CategoriesTable::getList()->fetchAll();
                    foreach ($categories as $category) : ?>
                        <option value="<?= htmlspecialchars($category['ID']) ?>"><?= htmlspecialchars($category['NAME']) ?></option>
                    <?php endforeach; ?>
                </select>
            </div>
            <div class="form-group">
                <label for="visibility">Видимость формы</label>
                <select id="visibility" name="VISIBILITY" required>
                    <option value="all">Все пользователи</option>
                    <option value="intranet">Интранет</option>
                    <option value="extranet">Экстранет</option>
                    <?php foreach ($projects as $projectId => $projectName): ?>
                        <option value="<?= $projectId ?>"><?= $projectName ?></option>
                    <?php endforeach; ?>
                </select>
            </div>
            <div class="form-group">
                <label for="group-visibility">Видимость по Группам пользователей</label>
                <select id="group-visibility" name="GROUP_VISIBILITY[]" multiple required>
                    <?php foreach ($userGroups as $groupId => $groupName): ?>
                        <option value="<?= $groupId ?>"><?= htmlspecialchars($groupName) ?></option>
                    <?php endforeach; ?>
                </select>
            </div>
            <div class="form-group">
                <label for="business-process-id">ID Бизнес-процесса</label>
                <input type="text" id="business-process-id" name="BUSINESS_PROCESS_ID" required>
            </div>
            <div class="form-group">
                <label for="process-id">ID Процесса</label>
                <input type="text" id="process-id" name="PROCESS_ID" required>
            </div>
            <div class="form-group">
                <label for="fields">Поля формы</label>
                <div id="fields-container" class="form-fields">
                    <!-- Поля будут добавляться сюда -->
                </div>
                <button type="button" id="add-field-btn">Добавить поле</button>
            </div>
            <button type="submit">Добавить форму</button>
        </form>

        <table class="form-table">
            <thead>
                <tr>
                    <th>ID</th>
                    <th>Название формы</th>
                    <th>Категория</th>
                    <th>Видимость</th>
                    <th>Видимость по Группам пользователей</th>
                    <th>ID Бизнес-процесса</th>
                    <th>ID Процесса</th>
                    <th>Поля формы</th>
                    <th>Действия</th>
                </tr>
            </thead>
            <tbody>
                <?php
                foreach ($filteredForms as $form) : 
                    $fields = json_decode($form['FIELDS'], true); ?>
                <tr data-form-id="<?= htmlspecialchars($form['ID']) ?>">
                    <td><?= htmlspecialchars($form['ID']) ?></td>
                    <td>
                        <span class="view-mode"><?= htmlspecialchars($form['NAME']) ?></span>
                        <input type="text" class="edit-mode form-name-input" value="<?= htmlspecialchars($form['NAME']) ?>" style="display:none;">
                    </td>
                    <td>
                        <span class="view-mode"><?= htmlspecialchars($categories[array_search($form['CATEGORY_ID'], array_column($categories, 'ID'))]['NAME']) ?></span>
                        <select class="edit-mode category-id-select" style="display:none;">
                            <?php foreach ($categories as $category): ?>
                                <option value="<?= htmlspecialchars($category['ID']) ?>" <?= $form['CATEGORY_ID'] === $category['ID'] ? 'selected' : '' ?>><?= htmlspecialchars($category['NAME']) ?></option>
                            <?php endforeach; ?>
                        </select>
                    </td>
                    <td>
                        <span class="view-mode"><?= htmlspecialchars($projects[$form['VISIBILITY']] ?? $form['VISIBILITY']) ?></span>
                        <select class="edit-mode visibility-select" style="display:none;">
                            <option value="all" <?= $form['VISIBILITY'] === 'all' ? 'selected' : '' ?>>Все пользователи</option>
                            <option value="intranet" <?= $form['VISIBILITY'] === 'intranet' ? 'selected' : '' ?>>Интранет</option>
                            <option value="extranet" <?= $form['VISIBILITY'] === 'extranet' ? 'selected' : '' ?>>Экстранет</option>
                            <?php foreach ($projects as $projectId => $projectName): ?>
                                <option value="<?= $projectId ?>" <?= $form['VISIBILITY'] === $projectId ? 'selected' : '' ?>><?= $projectName ?></option>
                            <?php endforeach; ?>
                        </select>
                    </td>
                    <td>
                        <span class="view-mode"><?= htmlspecialchars($form['GROUP_VISIBILITY']) ?></span>
                        <select class="edit-mode group-visibility-select" multiple style="display:none;">
                            <?php foreach ($userGroups as $groupId => $groupName): ?>
                                <option value="<?= $groupId ?>" <?= in_array($groupId, explode(',', $form['GROUP_VISIBILITY'])) ? 'selected' : '' ?>><?= htmlspecialchars($groupName) ?></option>
                            <?php endforeach; ?>
                        </select>
                    </td>
                    <td>
                        <span class="view-mode"><?= htmlspecialchars($form['BUSINESS_PROCESS_ID']) ?></span>
                        <input type="text" class="edit-mode business-process-id-input" value="<?= htmlspecialchars($form['BUSINESS_PROCESS_ID']) ?>" style="display:none;">
                    </td>
                    <td>
                        <span class="view-mode"><?= htmlspecialchars($form['PROCESS_ID']) ?></span>
                        <input type="text" class="edit-mode process-id-input" value="<?= htmlspecialchars($form['PROCESS_ID']) ?>" style="display:none;">
                    </td>
                    <td>
                        <span class="view-mode">
                            <?php foreach ($fields as $field) : ?>
                                <div><?= htmlspecialchars($field['name']) ?> (<?= htmlspecialchars($field['type']) ?>)</div>
                            <?php endforeach; ?>
                        </span>
                        <textarea class="edit-mode fields-input" style="display:none;"><?= htmlspecialchars(json_encode($fields, JSON_UNESCAPED_UNICODE)) ?></textarea>
                    </td>
                    <td>
                        <div class="actions">
                            <button type="button" class="edit-btn">Редактировать</button>
                            <button type="button" class="save-btn" style="display:none;">Сохранить</button>
                            <a href="?action=delete&ID=<?= htmlspecialchars($form['ID']) ?>">Удалить</a>
                        </div>
                    </td>
                </tr>
                <?php endforeach; ?>
            </tbody>
        </table>
    </div>
    <?php require($_SERVER['DOCUMENT_ROOT'] . "/bitrix/footer.php"); ?>
</body>
</html>
