<?php
require($_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/main/include/prolog_before.php');

use Bitrix\Main\Loader;
use Bitrix\Main\IO\Directory;
use Bitrix\Main\Application;

if (!Loader::includeModule('bizproc')) {
    echo "Модуль бизнес-процессов не загружен";
    die();
}

session_start();
$forms = $_SESSION['forms'] ?? [];
$formId = $_SESSION['formId'] ?? null;

if ($_SERVER['REQUEST_METHOD'] === 'POST') {
    $submittedData = [];
    foreach ($_POST['form_data'] as $key => $value) {
        $submittedData[$key] = htmlspecialchars($value);
    }

    if (isset($_FILES['form_data'])) {
        $uploadDir = $_SERVER['DOCUMENT_ROOT'] . '/support/files/';
        
        // Проверяем, существует ли директория, если нет, создаем
        if (!Directory::isDirectoryExists($uploadDir)) {
            Directory::createDirectory($uploadDir);
        }

        $file = $_FILES['form_data']['name']['file'] ?? null;
        $tmpName = $_FILES['form_data']['tmp_name']['file'] ?? null;

        if ($file && $tmpName) {
            $uploadFile = $uploadDir . basename($file);
            if (move_uploaded_file($tmpName, $uploadFile)) {
                $submittedData['file'] = 'http://' . $_SERVER['HTTP_HOST'] . '/support/files/' . basename($file);
            } else {
                $submittedData['file'] = 'Файл не загружен';
            }
        }
    }

    echo '<pre>Отправленные данные: ';
    foreach ($submittedData as $key => $value) {
        echo htmlspecialchars($key) . ': ' . htmlspecialchars($value) . '<br>';
    }
    echo '</pre>';

    $form = null;
    foreach ($forms as $f) {
        if ($f['ID'] == $formId) {
            $form = $f;
            break;
        }
    }

    if ($form) {
        $businessProcessId = $form['BUSINESS_PROCESS_ID'];
        $processId = $form['PROCESS_ID'];

        if ($businessProcessId) {
            $workflowParameters = [
                'FormData' => json_encode($submittedData, JSON_UNESCAPED_UNICODE),
            ];

            echo '<pre>Параметры бизнес-процесса: ';
            print_r($workflowParameters);
            echo '</pre>';

            try {
                $workflowId = CBPDocument::StartWorkflow(
                    $businessProcessId,
                    ["iblock", "CIBlockDocument", $processId],
                    $workflowParameters,
                    $errors
                );
                if (!empty($errors)) {
                    echo '<pre>Ошибки: ';
                    print_r($errors);
                    echo '</pre>';
                } else {
                    echo '<pre>Бизнес-процесс успешно запущен.</pre>';
                }
            } catch (Exception $e) {
                echo '<pre>Ошибка при запуске бизнес-процесса: ' . $e->getMessage() . '</pre>';
            }
        } else {
            echo '<pre>Ошибка: Бизнес-процесс не найден.</pre>';
        }
    } else {
        echo '<pre>Ошибка: Форма не найдена.</pre>';
    }
}
?>
