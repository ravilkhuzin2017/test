<?php
use Bitrix\Main\Localization\Loc;
Loc::loadMessages(__FILE__);

class custom_support extends CModule
{
    var $MODULE_ID = "custom.support";
    var $MODULE_VERSION;
    var $MODULE_VERSION_DATE;
    var $MODULE_NAME;
    var $MODULE_DESCRIPTION;
    var $PARTNER_NAME;
    var $PARTNER_URI;

    function __construct()
    {
        $arModuleVersion = [];
        include(__DIR__ . "/version.php");

        if (is_array($arModuleVersion) && array_key_exists("VERSION", $arModuleVersion))
        {
            $this->MODULE_VERSION = $arModuleVersion["VERSION"];
            $this->MODULE_VERSION_DATE = $arModuleVersion["VERSION_DATE"];
        }

        $this->MODULE_NAME = Loc::getMessage("CUSTOM_SUPPORT_MODULE_NAME");
        $this->MODULE_DESCRIPTION = Loc::getMessage("CUSTOM_SUPPORT_MODULE_DESCRIPTION");
        $this->PARTNER_NAME = Loc::getMessage("CUSTOM_SUPPORT_PARTNER_NAME");
        $this->PARTNER_URI = Loc::getMessage("CUSTOM_SUPPORT_PARTNER_URI");
    }

    function DoInstall()
    {
        global $APPLICATION;
        $this->InstallFiles();
        if ($this->InstallDB()) {
            RegisterModule($this->MODULE_ID);
            $APPLICATION->IncludeAdminFile(Loc::getMessage("CUSTOM_SUPPORT_INSTALL_TITLE"), __DIR__ . "/step.php");
        } else {
            $APPLICATION->IncludeAdminFile(Loc::getMessage("CUSTOM_SUPPORT_INSTALL_FAILED_TITLE"), __DIR__ . "/unstep.php");
        }
    }

    function DoUninstall()
    {
        global $APPLICATION;
        $this->UnInstallDB();
        $this->UnInstallFiles();
        UnRegisterModule($this->MODULE_ID);
        $APPLICATION->IncludeAdminFile(Loc::getMessage("CUSTOM_SUPPORT_UNINSTALL_TITLE"), __DIR__ . "/unstep.php");
    }

    function InstallFiles()
    {
        CopyDirFiles(__DIR__ . '/admin', $_SERVER['DOCUMENT_ROOT'] . '/bitrix/admin', true, true);
        CopyDirFiles(__DIR__ . '/public', $_SERVER['DOCUMENT_ROOT'], true, true);
        CopyDirFiles(__DIR__ . '/lang/ru', $_SERVER['DOCUMENT_ROOT'] . '/local/modules/custom.support/lang/ru', true, true);
        return true;
    }

    function UnInstallFiles()
    {
        DeleteDirFiles(__DIR__ . '/admin', $_SERVER['DOCUMENT_ROOT'] . '/bitrix/admin');
        DeleteDirFilesEx('/support/categories.php');
        DeleteDirFilesEx('/support/forms.php');
        DeleteDirFilesEx('/support/support.php');
        DeleteDirFilesEx('/support/load_form.php');
        DeleteDirFilesEx('/support/load_forms.php');
        DeleteDirFilesEx('/support/post.php');
        return true;
    }

    function InstallDB()
    {
        global $DB;
        $DB->RunSQLBatch(__DIR__ . '/db/install.sql');
        return true;
    }

    function UnInstallDB()
    {
        global $DB;
        $DB->RunSQLBatch(__DIR__ . '/db/uninstall.sql');
        return true;
    }
}
?>
