<?php
\Bitrix\Main\Loader::registerAutoloadClasses(
    'custom.support',
    array(
        'Custom\Support\CategoriesTable' => 'lib/CategoriesTable.php',
        'Custom\Support\FormsTable' => 'lib/FormsTable.php',
    )
);
?>
