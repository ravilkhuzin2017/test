<?php
if (!check_bitrix_sessid()) return;
echo CAdminMessage::ShowNote("Удаление модуля завершено.");
?>
<form action="<?php echo $APPLICATION->GetCurPage(); ?>">
    <input type="hidden" name="lang" value="<?php echo LANG; ?>">
    <input type="submit" value="Вернуться к списку">
</form>
