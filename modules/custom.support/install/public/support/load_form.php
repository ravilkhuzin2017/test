<?php
use Bitrix\Main\UserTable;
session_start();

$formId = $_GET['FORM_ID'] ?? null;
$_SESSION['formId'] = $formId;
$forms = $_SESSION['forms'] ?? [];
$employees = $_SESSION['employees'] ?? [];
$form = null;

foreach ($forms as $f) {
    if ($f['ID'] == $formId) {
        $form = $f;
        break;
    }
}

if ($form) {
    $fields = json_decode($form['FIELDS'], true);
    ?>
    <div class="back-button">&larr; Назад к списку форм</div>
    <form id='form' action="post.php" method="post" enctype="multipart/form-data">
        <h2><?= $form['NAME'] ?></h2>
        <?php foreach ($fields as $field) : ?>
            <div class="form-group">
                <label><?= htmlspecialchars($field['name']) ?></label>
                <?php if ($field['type'] === 'text') : ?>
                    <input type="text" name="form_data[<?= htmlspecialchars($field['name']) ?>]" placeholder="<?= htmlspecialchars($field['name']) ?>">
                <?php elseif ($field['type'] === 'textarea') : ?>
                    <textarea name="form_data[<?= htmlspecialchars($field['name']) ?>]" placeholder="<?= htmlspecialchars($field['name']) ?>"></textarea>
                <?php elseif ($field['type'] === 'yesno') : ?>
                    <select name="form_data[<?= htmlspecialchars($field['name']) ?>]">
                        <option value="yes">Да</option>
                        <option value="no">Нет</option>
                    </select>
                <?php elseif ($field['type'] === 'email') : ?>
                    <input type="email" name="form_data[<?= htmlspecialchars($field['name']) ?>]" placeholder="<?= htmlspecialchars($field['name']) ?>">
                <?php elseif ($field['type'] === 'number') : ?>
                    <input type="number" name="form_data[<?= htmlspecialchars($field['name']) ?>]" placeholder="<?= htmlspecialchars($field['name']) ?>">
                <?php elseif ($field['type'] === 'date') : ?>
                    <input type="date" name="form_data[<?= htmlspecialchars($field['name']) ?>]" placeholder="<?= htmlspecialchars($field['name']) ?>">
                <?php elseif ($field['type'] === 'datetime') : ?>
                    <input type="datetime-local" name="form_data[<?= htmlspecialchars($field['name']) ?>]" placeholder="<?= htmlspecialchars($field['name']) ?>">
                    <?php elseif ($field['type'] === 'employee') : ?>
    <select name="form_data[<?= htmlspecialchars($field['name']) ?>]">
        <?php
        foreach ($employees as $employee) {
            $employeeFullName = htmlspecialchars($employee['NAME'] . ' ' . $employee['LAST_NAME']);
            echo '<option value="' . $employeeFullName . '">' . $employeeFullName . '</option>';
        }
        ?>
    </select>
                <?php elseif ($field['type'] === 'file') : ?>
                    <input type="file" name="form_data[file]">
                <?php elseif ($field['type'] === 'list') : ?>
                    <select name="form_data[<?= htmlspecialchars($field['name']) ?>]">
                        <?php
                        $options = explode(',', $field['options']);
                        foreach ($options as $option) {
                            echo '<option value="' . htmlspecialchars($option) . '">' . htmlspecialchars($option) . '</option>';
                        }
                        ?>
                    </select>
                <?php endif; ?>
            </div>
        <?php endforeach; ?>
        <button type="submit">Отправить</button>
    </form>
    <?php
} else {
    echo '<p>Форма не найдена.</p>';
}
?>
