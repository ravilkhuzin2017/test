<?php
require ($_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php");
use Bitrix\Main\Loader;
use Bitrix\Main\Application;
use Custom\Support\FormsTable;
use Custom\Support\CategoriesTable;
use Bitrix\Socialnetwork\WorkgroupTable;
use Bitrix\Socialnetwork\UserToGroupTable;
use Bitrix\Main\UserTable;
use Bitrix\Main\IO\Directory;
use Bitrix\Main\IO\File;
use Bitrix\Bizproc\Workflow\Starter;

// Проверка загрузки модулей
if (!Loader::includeModule('custom.support')) {
    echo "Модуль custom.support не загружен";
    die();
}

if (!Loader::includeModule('socialnetwork')) {
    echo "Модуль socialnetwork не загружен";
    die();
}

if (!Loader::includeModule('bizproc')) {
    echo "Модуль бизнес-процессов не загружен";
    die();
}
session_start();
// Получение реальных проектов из Битрикс
$projects = [];
$rsGroups = WorkgroupTable::getList([
    'select' => ['ID', 'NAME']
]);
while ($arGroup = $rsGroups->fetch()) {
    $projects[$arGroup['ID']] = $arGroup['NAME'];
}

$request = Application::getInstance()->getContext()->getRequest();
$categoryId = $request->get('CATEGORY_ID');
$errors = [];

// Получение групп текущего пользователя
global $USER;
$userGroups = $USER->GetUserGroupArray();
$_SESSION['userGroups'] = $userGroups;
$userId = $USER->GetID();
$userProjects = [];
$rsUserProjects = UserToGroupTable::getList([
    'select' => ['GROUP_ID'],
    'filter' => ['=USER_ID' => $userId]
]);
while ($arUserProject = $rsUserProjects->fetch()) {
    $userProjects[] = $arUserProject['GROUP_ID'];
    $_SESSION['userProjects'] = $userProjects;
}

// Фильтрация категорий по видимости
function filterCategoriesByVisibility($categories, $userGroups, $userProjects)
{
    return array_filter($categories, function ($category) use ($userGroups, $userProjects) {
        if ($category['VISIBILITY'] === 'all') {
            return true;
        }
        if ($category['VISIBILITY'] === 'intranet' && in_array(2, $userGroups)) {
            return true;
        }
        if ($category['VISIBILITY'] === 'extranet' && in_array(1, $userGroups)) {
            return true;
        }
        if (in_array($category['VISIBILITY'], $userProjects)) {
            return true;
        }
        return false;
    });
}

$categories = CategoriesTable::getList()->fetchAll();
$filteredCategories = filterCategoriesByVisibility($categories, $userGroups, $userProjects);
$forms = [];

$forms = FormsTable::getList()->fetchAll();

$_SESSION['forms'] = $forms;
$employees = UserTable::getList([
    'select' => ['ID', 'NAME', 'LAST_NAME'],
    'filter' => ['ACTIVE' => 'Y']
])->fetchAll();

$_SESSION['employees'] = $employees;

?>

<!DOCTYPE html>
<html lang="ru">

<head>
    <meta charset="UTF-8">
    <title>Техническая поддержка</title>
    <style>
        body {
            font-family: Arial, sans-serif;
        }

        .container {
            display: flex;
            max-width: 1000px;
            margin: 0 auto;
            padding: 20px;
        }

        .left,
        .right {
            padding: 20px;
        }

        .left {
            width: 30%;
        }

        .right {
            width: 70%;
        }

        .form-group {
            margin-bottom: 15px;
        }

        .form-group label {
            display: block;
            margin-bottom: 5px;
        }

        .form-group select,
        .form-group input,
        .form-group textarea {
            width: 100%;
            padding: 8px;
            box-sizing: border-box;
        }

        .category-list {
            list-style: none;
            padding: 0;
        }

        .category-list li {
            margin-bottom: 10px;
        }

        .form-list {
            list-style: none;
            padding: 0;
        }

        .form-list li {
            margin-bottom: 10px;
        }

        .form-container,
        .form-list-container {
            display: none;
        }

        .back-button {
            display: inline-block;
            margin-bottom: 20px;
            cursor: pointer;
            color: blue;
            text-decoration: underline;
        }
    </style>
    <script>
        document.addEventListener("DOMContentLoaded", function () {
            var formId = "<?= $formId ?>";
            var formContainer = document.querySelector(".form-container");
            var formListContainer = document.querySelector(".form-list");

            if (formId) {
                formContainer.style.display = "block";
                formListContainer.style.display = "none";
            } else {
                formContainer.style.display = "none";
                formListContainer.style.display = "block";
            }

            document.querySelector(".back-button").addEventListener("click", function () {
                formContainer.style.display = "none";
                formListContainer.style.display = "block";
            });
        });
    </script>
</head>

<body>
    <div class="container">
        <div class="left">
            <h1>Категории</h1>
            <ul class="category-list">
                <?php foreach ($filteredCategories as $category): ?>
                    <li>
                        <a href="?CATEGORY_ID=<?= $category['ID'] ?>" <?= $categoryId == $category['ID'] ? 'class="active"' : '' ?>>
                            <?php if (!empty($category['ICON'])): ?>
                                <img src="/support/icons/<?= htmlspecialchars($category['ICON']) ?>"
                                    alt="<?= htmlspecialchars($category['NAME']) ?>" style="width: 24px; height: 24px;">
                            <?php endif; ?>
                            <?= $category['NAME'] ?>
                        </a>
                    </li>
                <?php endforeach; ?>
            </ul>
        </div>
        <div class="right">
            <h1>Формы</h1>
            <div class="form-list" id="form-list-container">

            </div>
            <div class="form-container">
                <div class="back-button">&larr; Назад к списку форм</div>
            </div>
        </div>
    </div>
    <script>
        // JavaScript код для динамической загрузки форм
        document.addEventListener('DOMContentLoaded', function () {
            document.querySelector(".back-button").addEventListener("click", function () {
                formContainer.style.display = "none";
                formListContainer.style.display = "block";
            });

            var categoryLinks = document.querySelectorAll('.category-list a');
            console.log(categoryLinks)

            categoryLinks.forEach(function (link) {
                link.addEventListener('click', function (event) {
                    event.preventDefault(); // Отменяем действие по умолчанию (переход по ссылке)

                    var categoryId = link.getAttribute('href').split('=')[1]; // Получаем ID категории
                    loadForms(categoryId); // Загружаем формы для выбранной категории
                });
            });

            function loadForms(categoryId) {
                var xhr = new XMLHttpRequest();
                xhr.onreadystatechange = function () {
                    if (xhr.readyState === XMLHttpRequest.DONE) {
                        if (xhr.status === 200) {
                            var formListContainer = document.getElementById('form-list-container');
                            formListContainer.innerHTML = xhr.responseText;
                            var formLinks = document.querySelectorAll('.form-list a');
                            console.log(formLinks)
                            formLinks.forEach(function (link) {
                                link.addEventListener('click', function (event) {
                                    event.preventDefault(); // Отменяем действие по умолчанию (переход по ссылке)

                                    var formId = link.getAttribute('data-form-id'); // Получаем ID формы
                                    loadForm(formId);
                                });
                            });

                            function loadForm(formId) {
                                var xhr = new XMLHttpRequest();
                                xhr.onreadystatechange = function () {
                                    if (xhr.readyState === XMLHttpRequest.DONE) {
                                        if (xhr.status === 200) {
                                            var formContainer = document.getElementsByClassName('form-container');
                                            formContainer[0].innerHTML = xhr.responseText; // Заменяем содержимое контейнера формы новым HTML
                                            var formContainer = document.querySelector(".form-container");
                                            var formListContainer = document.querySelector(".form-list");
                                            formContainer.style.display = "block";
                                            formListContainer.style.display = "none";
                                            document.querySelector(".back-button").addEventListener("click", function () {
                                                formContainer.style.display = "none";
                                                formListContainer.style.display = "block";
                                            });
                                            var form = document.querySelector('#form');
                                            console.log(form)
                                            if (form) {
                                                console.log(form)

                                                form.addEventListener('submit', function (e) {
                                                    e.preventDefault();

                                                    var formData = new FormData(form); // Создаем объект FormData из формы
                                                    // Отправляем данные на сервер
                                                    var xhrPost = new XMLHttpRequest();
                                                    xhrPost.onreadystatechange = function () {
                                                        if (xhrPost.readyState === XMLHttpRequest.DONE) {
                                                            if (xhrPost.status === 200) {
                                                                console.log(xhrPost.responseText); // Выводим ответ сервера в консоль
                                                                var container = document.querySelector('.container');
                                                                var logPopup = document.querySelector('.log-popup');
                                                                var work = document.querySelector('.workarea-content-paddings');
                                                                var back = document.querySelector(".back-button")
                                                                if (work) {
                                                                    work.innerHTML = "Ваш запрос отправлен в техническую поддержку";
                                                                    back.id = "additionalBlock";
                                                                    work.appendChild(back);
                                                                    document.querySelector("#additionalBlock").addEventListener("click", function () {
                                                                        location.reload();
                                                                    });
                                                                }
                                                                // Скрыть блок с классом container
                                                                if (container) {
                                                                    container.style.display = 'none';
                                                                }

                                                                // Добавить содержимое xhrPost.responseText в log-popup
                                                                if (logPopup) {
                                                                    logPopup.innerHTML = xhrPost.responseText;
                                                                }
                                                            } else {
                                                                console.error('Произошла ошибка при отправке формы: ' + xhrPost.status);
                                                            }
                                                        }
                                                    };
                                                    xhrPost.open('POST', 'post.php', true);
                                                    xhrPost.send(formData);
                                                });
                                            }

                                        } else {
                                            console.error('Произошла ошибка при загрузке формы: ' + xhr.status);
                                        }
                                    }
                                };

                                xhr.open('GET', 'load_form.php?FORM_ID=' + formId, true);
                                xhr.send();
                            };
                        } else {
                            console.error('Произошла ошибка при загрузке форм: ' + xhr.status);
                        }
                    }
                };

                xhr.open('GET', 'load_forms.php?CATEGORY_ID=' + categoryId, true);
                xhr.send();
            }
        });
    </script>

    <?php require ($_SERVER["DOCUMENT_ROOT"] . "/bitrix/footer.php"); ?>
</body>

</html>