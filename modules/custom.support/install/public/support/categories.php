<?php
require_once $_SERVER['DOCUMENT_ROOT'] . '/bitrix/modules/main/include/prolog_before.php';
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
use Bitrix\Main\Loader;
use Bitrix\Main\Application;
use Custom\Support\CategoriesTable;
use Bitrix\Socialnetwork\WorkgroupTable;

if (!Loader::includeModule('custom.support')) {
    echo "Модуль не загружен";
    die();
}

if (!Loader::includeModule('socialnetwork')) {
    echo "Модуль Socialnetwork не загружен";
    die();
}

// Получение реальных проектов из Битрикс
$projects = [];
$rsGroups = WorkgroupTable::getList([
    'select' => ['ID', 'NAME']
]);
while ($arGroup = $rsGroups->fetch()) {
    $projects[$arGroup['ID']] = $arGroup['NAME'];
}

// Получение списка иконок
$iconDir = $_SERVER['DOCUMENT_ROOT'] . '/support/icons/';
$icons = array_diff(scandir($iconDir), array('..', '.'));

$request = Application::getInstance()->getContext()->getRequest();
$action = $request->get('action');
$errors = [];

if ($action === 'add' || $action === 'edit') {
    $categoryId = $request->get('ID');
    $categoryName = $request->getPost('NAME');
    $visibility = $request->getPost('VISIBILITY');
    $icon = $request->getPost('ICON');

    if (empty($categoryName)) {
        $errors[] = 'Название категории не может быть пустым';
    }

    if (empty($errors)) {
        if ($action === 'add') {
            $result = CategoriesTable::add([
                'NAME' => $categoryName,
                'VISIBILITY' => $visibility,
                'ICON' => $icon,
            ]);

            if (!$result->isSuccess()) {
                $errors = array_merge($errors, $result->getErrorMessages());
            } else {
                echo '<pre>';
                echo "Категория успешно добавлена\n";
                echo '</pre>';
            }
        } elseif ($action === 'edit' && $categoryId) {
            $result = CategoriesTable::update($categoryId, [
                'NAME' => $categoryName,
                'VISIBILITY' => $visibility,
                'ICON' => $icon,
            ]);

            if (!$result->isSuccess()) {
                $errors = array_merge($errors, $result->getErrorMessages());
            } else {
                echo '<pre>';
                echo "Категория успешно обновлена\n";
                echo '</pre>';
            }
        }

        if (empty($errors)) {
            LocalRedirect('/support/categories.php');
        }
    }
}

if ($action === 'delete' && $request->get('ID')) {
    $categoryId = $request->get('ID');
    CategoriesTable::delete($categoryId);
    LocalRedirect('/support/categories.php');
}

// Выводим ошибки
foreach ($errors as $error) {
    echo '<div class="ui-alert ui-alert-danger">' . $error . '</div>';
}

$categories = CategoriesTable::getList()->fetchAll();
?>

<!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <title>Категории поддержки</title>
    <style>
        body { font-family: Arial, sans-serif; }
        .container { max-width: 600px; margin: 0 auto; padding: 20px; }
        .form-group { margin-bottom: 15px; }
        .form-group label { display: block; margin-bottom: 5px; }
        .form-group input, .form-group select { width: 100%; padding: 8px; box-sizing: border-box; }
        table { width: 100%; border-collapse: collapse; margin-top: 20px; }
        .category-table, .category-table th, .category-table td {
            border: 1px solid #ddd;
        }
        .category-table th, .category-table td {
            padding: 10px; 
            text-align: left;
        }
        .category-table th {
            background-color: #f4f4f4;
        }
        .actions { display: flex; gap: 10px; }
        .edit-mode { display: none; }
        .icon-preview {
            width: 32px;
            height: 32px;
        }
    </style>
    <script>
        document.addEventListener("DOMContentLoaded", function () {
            document.querySelectorAll(".edit-btn").forEach(function (button) {
                button.addEventListener("click", function () {
                    var row = this.closest("tr");
                    row.querySelector(".view-mode").style.display = "none";
                    row.querySelectorAll(".edit-mode").forEach(function (el) {
                        el.style.display = "block";
                    });
                    this.style.display = "none";
                    row.querySelector(".save-btn").style.display = "inline-block";
                });
            });

            document.querySelectorAll(".save-btn").forEach(function (button) {
                button.addEventListener("click", function () {
                    var row = this.closest("tr");
                    var categoryId = row.dataset.categoryId;
                    var categoryName = row.querySelector(".category-name-input").value;
                    var visibility = row.querySelector(".visibility-select").value;
                    var icon = row.querySelector(".icon-select").value;

                    var xhr = new XMLHttpRequest();
                    xhr.open("POST", "/support/categories.php?action=edit&ID=" + categoryId, true);
                    xhr.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
                    xhr.onreadystatechange = function () {
                        if (xhr.readyState === 4 && xhr.status === 200) {
                            location.reload();
                        }
                    };
                    xhr.send("NAME=" + encodeURIComponent(categoryName) + "&VISIBILITY=" + encodeURIComponent(visibility) + "&ICON=" + encodeURIComponent(icon));
                });
            });
        });
    </script>
</head>
<body>
    <div class="container">
        <h1>Категории поддержки</h1>

        <form method="post" action="?action=add">
            <div class="form-group">
                <label for="category-name">Название категории</label>
                <input type="text" id="category-name" name="NAME" required>
            </div>
            <div class="form-group">
                <label for="visibility">Видимость категории</label>
                <select id="visibility" name="VISIBILITY" required>
                    <option value="all">Все пользователи</option>
                    <option value="intranet">Интранет</option>
                    <option value="extranet">Экстранет</option>
                    <?php foreach ($projects as $projectId => $projectName): ?>
                        <option value="<?= $projectId ?>"><?= $projectName ?></option>
                    <?php endforeach; ?>
                </select>
            </div>
            <div class="form-group">
                <label for="icon">Иконка категории</label>
                <select id="icon" name="ICON" required>
                    <?php foreach ($icons as $icon): ?>
                        <option value="<?= htmlspecialchars($icon) ?>"><?= htmlspecialchars($icon) ?></option>
                    <?php endforeach; ?>
                </select>
            </div>
            <button type="submit">Добавить категорию</button>
        </form>

        <table class="category-table">
            <thead>
                <tr>
                    <th>ID</th>
                    <th>Название категории</th>
                    <th>Видимость</th>
                    <th>Иконка</th>
                    <th>Действия</th>
                </tr>
            </thead>
            <tbody>
                <?php if (empty($categories)): ?>
                    <tr>
                        <td colspan="5">Нет доступных категорий</td>
                    </tr>
                <?php else: ?>
                    <?php foreach ($categories as $category): ?>
                    <tr data-category-id="<?= $category['ID'] ?>">
                        <td><?= $category['ID'] ?></td>
                        <td>
                            <span class="view-mode"><?= htmlspecialchars($category['NAME']) ?></span>
                            <input type="text" class="edit-mode category-name-input" value="<?= htmlspecialchars($category['NAME']) ?>" style="display:none;">
                        </td>
                        <td>
                            <span class="view-mode"><?= htmlspecialchars($projects[$category['VISIBILITY']] ?? $category['VISIBILITY']) ?></span>
                            <select class="edit-mode visibility-select" style="display:none;">
                                <option value="all" <?= $category['VISIBILITY'] === 'all' ? 'selected' : '' ?>>Все пользователи</option>
                                <option value="intranet" <?= $category['VISIBILITY'] === 'intranet' ? 'selected' : '' ?>>Интранет</option>
                                <option value="extranet" <?= $category['VISIBILITY'] === 'extranet' ? 'selected' : '' ?>>Экстранет</option>
                                <?php foreach ($projects as $projectId => $projectName): ?>
                                    <option value="<?= $projectId ?>" <?= $category['VISIBILITY'] === $projectId ? 'selected' : '' ?>><?= $projectName ?></option>
                                <?php endforeach; ?>
                            </select>
                        </td>
                        <td>
                            <span class="view-mode"><img src="/support/icons/<?= htmlspecialchars($category['ICON']) ?>" class="icon-preview"></span>
                            <select class="edit-mode icon-select" style="display:none;">
                                <?php foreach ($icons as $icon): ?>
                                    <option value="<?= htmlspecialchars($icon) ?>" <?= $category['ICON'] === $icon ? 'selected' : '' ?>><?= htmlspecialchars($icon) ?></option>
                                <?php endforeach; ?>
                            </select>
                        </td>
                        <td>
                            <div class="actions">
                                <button type="button" class="edit-btn">Редактировать</button>
                                <button type="button" class="save-btn" style="display:none;">Сохранить</button>
                                <a href="?action=delete&ID=<?= $category['ID'] ?>">Удалить</a>
                            </div>
                        </td>
                    </tr>
                    <?php endforeach; ?>
                <?php endif; ?>
            </tbody>
        </table>
    </div>
    <?php require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/footer.php"); ?>
</body>
</html>
