<?php
function filterFormsByVisibility($forms, $userGroups, $userProjects) {
    return array_filter($forms, function($form) use ($userGroups, $userProjects) {
        if ($form['VISIBILITY'] === 'all') {
            return true;
        }
        if ($form['VISIBILITY'] === 'intranet' && in_array(2, $userGroups)) {
            return true;
        }
        if ($form['VISIBILITY'] === 'extranet' && in_array(1, $userGroups)) {
            return true;
        }
        if (in_array($form['VISIBILITY'], $userProjects)) {
            return true;
        }
        if (!empty($form['GROUP_VISIBILITY'])) {
            $groupVisibilities = explode(',', $form['GROUP_VISIBILITY']);
            foreach ($groupVisibilities as $groupId) {
                if (in_array((int)$groupId, $userGroups)) {
                    return true;
                }
            }
        }
        return false;
    });
}

session_start();

$categoryId = $_GET['CATEGORY_ID'] ?? null;
$forms = $_SESSION['forms'] ?? [];
$userGroups = $_SESSION['userGroups'] ?? [];
$userProjects = $_SESSION['userProjects'] ?? [];

if ($categoryId) {
    if (isset($forms) && is_array($forms)) {
        $filteredForms = array_filter($forms, function ($form) use ($categoryId) {
            return $form['CATEGORY_ID'] == $categoryId;
        });

        if (!empty($filteredForms)) {
            echo '<ul class="form-list">';
            foreach ($filteredForms as $form) {
                echo '<li><a href="#' . $categoryId . '&FORM_ID=' . $form['ID'] . '" data-form-id="' . $form['ID'] . '" class="load-form">' . $form['NAME'] . '</a></li>';
            }
            echo '</ul>';
        } else {
            echo '<p>Формы для выбранной категории отсутствуют.</p>';
        }
    } else {
        echo '<p>Формы не загружены. Проверьте настройки приложения.</p>';
    }
} else {
    echo '<p>Не указан CATEGORY_ID для загрузки форм.</p>';
}
?>
