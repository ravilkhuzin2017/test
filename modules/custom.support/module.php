<?php
use Bitrix\Main\Localization\Loc;

Loc::loadMessages(__FILE__);

if (class_exists('custom_support')) {
    return;
}

class custom_support extends CModule
{
    public $MODULE_ID = 'custom.support';
    public $MODULE_NAME;
    public $MODULE_DESCRIPTION;
    public $PARTNER_NAME;
    public $PARTNER_URI;

    public function __construct()
    {
        $arModuleVersion = [];
        include(__DIR__ . '/version.php');

        $this->MODULE_VERSION = $arModuleVersion['VERSION'];
        $this->MODULE_VERSION_DATE = $arModuleVersion['VERSION_DATE'];
        $this->MODULE_NAME = Loc::getMessage('CUSTOM_SUPPORT_MODULE_NAME');
        $this->MODULE_DESCRIPTION = Loc::getMessage('CUSTOM_SUPPORT_MODULE_DESCRIPTION');
        $this->PARTNER_NAME = Loc::getMessage('CUSTOM_SUPPORT_PARTNER_NAME');
        $this->PARTNER_URI = Loc::getMessage('CUSTOM_SUPPORT_PARTNER_URI');
    }

    public function doInstall()
    {
        global $APPLICATION;
        $this->installFiles();
        if ($this->installDB()) {
            RegisterModule($this->MODULE_ID);
            $APPLICATION->IncludeAdminFile(Loc::getMessage('CUSTOM_SUPPORT_INSTALL_TITLE'), __DIR__ . '/step.php');
        } else {
            $APPLICATION->IncludeAdminFile(Loc::getMessage('CUSTOM_SUPPORT_INSTALL_FAILED_TITLE'), __DIR__ . '/unstep.php');
        }
    }

    public function doUninstall()
    {
        global $APPLICATION;
        $this->uninstallDB();
        $this->uninstallFiles();
        UnRegisterModule($this->MODULE_ID);
        $APPLICATION->IncludeAdminFile(Loc::getMessage('CUSTOM_SUPPORT_UNINSTALL_TITLE'), __DIR__ . '/unstep.php');
    }

    public function installFiles()
    {
        CopyDirFiles(__DIR__ . '/admin', $_SERVER['DOCUMENT_ROOT'] . '/bitrix/admin', true, true);
        CopyDirFiles(__DIR__ . '/install/public', $_SERVER['DOCUMENT_ROOT'], true, true);
        return true;
    }

    public function uninstallFiles()
    {
        DeleteDirFiles(__DIR__ . '/admin', $_SERVER['DOCUMENT_ROOT'] . '/bitrix/admin');
        DeleteDirFilesEx('/categories.php');
        DeleteDirFilesEx('/forms.php');
        return true;
    }

    public function installDB()
    {
        global $DB, $APPLICATION;

        $DB->RunSQLBatch(__DIR__ . '/db/install.sql');
        return true;
    }

    public function uninstallDB()
    {
        global $DB, $APPLICATION;

        $DB->RunSQLBatch(__DIR__ . '/db/uninstall.sql');
        return true;
    }
}
?>
