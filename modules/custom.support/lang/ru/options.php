<?php
$MESS['CUSTOM_SUPPORT_CATEGORIES_TITLE'] = 'Категории поддержки';
$MESS['CUSTOM_SUPPORT_CATEGORIES_NAME'] = 'Название категории';
$MESS['CUSTOM_SUPPORT_CATEGORIES_ADD'] = 'Добавить категорию';
$MESS['CUSTOM_SUPPORT_CATEGORIES_EDIT'] = 'Редактировать';
$MESS['CUSTOM_SUPPORT_CATEGORIES_DELETE'] = 'Удалить';
$MESS['CUSTOM_SUPPORT_ERROR_CATEGORY_NAME'] = 'Название категории не может быть пустым';
$MESS['CUSTOM_SUPPORT_FORMS_TITLE'] = 'Формы поддержки';
$MESS['CUSTOM_SUPPORT_FORMS_NAME'] = 'Название формы';
$MESS['CUSTOM_SUPPORT_FORMS_CATEGORY'] = 'Категория';
$MESS['CUSTOM_SUPPORT_FORMS_FIELDS'] = 'Поля формы';
$MESS['CUSTOM_SUPPORT_FORMS_ADD'] = 'Добавить форму';
$MESS['CUSTOM_SUPPORT_FORMS_EDIT'] = 'Редактировать';
$MESS['CUSTOM_SUPPORT_FORMS_DELETE'] = 'Удалить';
$MESS['CUSTOM_SUPPORT_ERROR_FORM_NAME'] = 'Название формы не может быть пустым';
$MESS['CUSTOM_SUPPORT_ERROR_FORM_CATEGORY'] = 'Выберите категорию для формы';
?>
