<?php
$MESS['CUSTOM_SUPPORT_CATEGORIES_TITLE'] = 'Категории поддержки';
$MESS['CUSTOM_SUPPORT_CATEGORIES_NAME'] = 'Название категории';
$MESS['CUSTOM_SUPPORT_CATEGORIES_ADD'] = 'Добавить категорию';
$MESS['CUSTOM_SUPPORT_CATEGORIES_EDIT'] = 'Редактировать';
$MESS['CUSTOM_SUPPORT_CATEGORIES_DELETE'] = 'Удалить';
$MESS['CUSTOM_SUPPORT_ERROR_CATEGORY_NAME'] = 'Название категории не может быть пустым';
?>
