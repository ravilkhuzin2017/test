<?php
require_once $_SERVER['DOCUMENT_ROOT'] . '/bitrix/modules/main/include/prolog_admin_before.php';
use Bitrix\Main\Localization\Loc;
use Bitrix\Main\Loader;
use Bitrix\Main\Application;
use Custom\Support\CategoriesTable;

Loc::loadMessages(__FILE__);

if (!Loader::includeModule('custom.support')) {
    return;
}

$request = Application::getInstance()->getContext()->getRequest();
$action = $request->get('action');
$errors = [];

if ($action === 'add' || $action === 'edit') {
    $categoryId = $request->get('ID');
    $categoryName = $request->getPost('NAME');

    if (empty($categoryName)) {
        $errors[] = Loc::getMessage('CUSTOM_SUPPORT_ERROR_CATEGORY_NAME');
    }

    if (empty($errors)) {
        if ($action === 'add') {
            CategoriesTable::add([
                'NAME' => $categoryName,
            ]);
        } elseif ($action === 'edit' && $categoryId) {
            CategoriesTable::update($categoryId, [
                'NAME' => $categoryName,
            ]);
        }

        LocalRedirect('/bitrix/admin/categories.php');
    }
}

if ($action === 'delete' && $request->get('ID')) {
    $categoryId = $request->get('ID');
    CategoriesTable::delete($categoryId);
    LocalRedirect('/bitrix/admin/categories.php');
}

// Выводим ошибки
foreach ($errors as $error) {
    echo '<div class="ui-alert ui-alert-danger">' . $error . '</div>';
}
?>

<h1><?= Loc::getMessage('CUSTOM_SUPPORT_CATEGORIES_TITLE') ?></h1>

<form method="post">
    <input type="hidden" name="action" value="add">
    <div>
        <label for="category-name"><?= Loc::getMessage('CUSTOM_SUPPORT_CATEGORIES_NAME') ?></label>
        <input type="text" id="category-name" name="NAME">
    </div>
    <button type="submit"><?= Loc::getMessage('CUSTOM_SUPPORT_CATEGORIES_ADD') ?></button>
</form>

<table>
    <thead>
        <tr>
            <th>ID</th>
            <th><?= Loc::getMessage('CUSTOM_SUPPORT_CATEGORIES_NAME') ?></th>
            <th></th>
        </tr>
    </thead>
    <tbody>
        <?php
        $categories = CategoriesTable::getList()->fetchAll();
        foreach ($categories as $category) : ?>
        <tr>
            <td><?= $category['ID'] ?></td>
            <td><?= $category['NAME'] ?></td>
            <td>
                <a href="?action=edit&ID=<?= $category['ID'] ?>"><?= Loc::getMessage('CUSTOM_SUPPORT_CATEGORIES_EDIT') ?></a>
                <a href="?action=delete&ID=<?= $category['ID'] ?>"><?= Loc::getMessage('CUSTOM_SUPPORT_CATEGORIES_DELETE') ?></a>
            </td>
        </tr>
        <?php endforeach; ?>
    </tbody>
</table>

<?php require $_SERVER['DOCUMENT_ROOT'] . '/bitrix/modules/main/include/epilog_admin.php'; ?>
