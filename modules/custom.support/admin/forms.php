<?php
require_once $_SERVER['DOCUMENT_ROOT'] . '/bitrix/modules/main/include/prolog_admin_before.php';
use Bitrix\Main\Localization\Loc;
use Bitrix\Main\Loader;
use Bitrix\Main\Application;
use Bitrix\Main\Web\Json;
use Custom\Support\FormsTable;
use Custom\Support\CategoriesTable;

Loc::loadMessages(__FILE__);

if (!Loader::includeModule('custom.support')) {
    return;
}

$request = Application::getInstance()->getContext()->getRequest();
$action = $request->get('action');
$errors = [];

if ($action === 'add' || $action === 'edit') {
    $formId = $request->get('ID');
    $formName = $request->getPost('NAME');
    $categoryId = $request->getPost('CATEGORY_ID');
    $fields = $request->getPost('FIELDS');
    $fields = Json::encode($fields);

    if (empty($formName)) {
        $errors[] = Loc::getMessage('CUSTOM_SUPPORT_ERROR_FORM_NAME');
    }

    if (empty($categoryId)) {
        $errors[] = Loc::getMessage('CUSTOM_SUPPORT_ERROR_FORM_CATEGORY');
    }

    if (empty($errors)) {
        if ($action === 'add') {
            FormsTable::add([
                'NAME' => $formName,
                'CATEGORY_ID' => $categoryId,
                'FIELDS' => $fields,
            ]);
        } elseif ($action === 'edit' && $formId) {
            FormsTable::update($formId, [
                'NAME' => $formName,
                'CATEGORY_ID' => $categoryId,
                'FIELDS' => $fields,
            ]);
        }

        LocalRedirect('/bitrix/admin/forms.php');
    }
}

if ($action === 'delete' && $request->get('ID')) {
    $formId = $request->get('ID');
    FormsTable::delete($formId);
    LocalRedirect('/bitrix/admin/forms.php');
}

// Выводим ошибки
foreach ($errors as $error) {
    echo '<div class="ui-alert ui-alert-danger">' . $error . '</div>';
}
?>

<h1><?= Loc::getMessage('CUSTOM_SUPPORT_FORMS_TITLE') ?></h1>

<form method="post">
    <input type="hidden" name="action" value="add">
    <div>
        <label for="form-name"><?= Loc::getMessage('CUSTOM_SUPPORT_FORMS_NAME') ?></label>
        <input type="text" id="form-name" name="NAME">
    </div>
    <div>
        <label for="form-category"><?= Loc::getMessage('CUSTOM_SUPPORT_FORMS_CATEGORY') ?></label>
        <select id="form-category" name="CATEGORY_ID">
            <?php
            $categories = CategoriesTable::getList()->fetchAll();
            foreach ($categories as $category) {
                echo '<option value="' . $category['ID'] . '">' . $category['NAME'] . '</option>';
            }
            ?>
        </select>
    </div>
    <div>
        <label for="form-fields"><?= Loc::getMessage('CUSTOM_SUPPORT_FORMS_FIELDS') ?></label>
        <textarea id="form-fields" name="FIELDS"></textarea>
    </div>
    <button type="submit"><?= Loc::getMessage('CUSTOM_SUPPORT_FORMS_ADD') ?></button>
</form>

<table>
    <thead>
        <tr>
            <th>ID</th>
            <th><?= Loc::getMessage('CUSTOM_SUPPORT_FORMS_NAME') ?></th>
            <th><?= Loc::getMessage('CUSTOM_SUPPORT_FORMS_CATEGORY') ?></th>
            <th><?= Loc::getMessage('CUSTOM_SUPPORT_FORMS_FIELDS') ?></th>
            <th></th>
        </tr>
    </thead>
    <tbody>
        <?php
        $forms = FormsTable::getList()->fetchAll();
        foreach ($forms as $form) : ?>
        <tr>
            <td><?= $form['ID'] ?></td>
            <td><?= $form['NAME'] ?></td>
            <td><?= $form['CATEGORY_ID'] ?></td>
            <td><?= $form['FIELDS'] ?></td>
            <td>
                <a href="?action=edit&ID=<?= $form['ID'] ?>"><?= Loc::getMessage('CUSTOM_SUPPORT_FORMS_EDIT') ?></a>
                <a href="?action=delete&ID=<?= $form['ID'] ?>"><?= Loc::getMessage('CUSTOM_SUPPORT_FORMS_DELETE') ?></a>
            </td>
        </tr>
        <?php endforeach; ?>
    </tbody>
</table>

<?php require $_SERVER['DOCUMENT_ROOT'] . '/bitrix/modules/main/include/epilog_admin.php'; ?>
