<?php
namespace Custom\Support;

use Bitrix\Main\Entity;
use Bitrix\Main\Localization\Loc;

Loc::loadMessages(__FILE__);

class FormsTable extends Entity\DataManager
{
    public static function getTableName()
    {
        return 'custom_support_forms';
    }

    public static function getMap()
    {
        return [
            new Entity\IntegerField('ID', [
                'primary' => true,
                'autocomplete' => true,
            ]),
            new Entity\StringField('NAME', [
                'required' => true,
                'validation' => function() {
                    return [
                        new Entity\Validator\Length(null, 255),
                    ];
                }
            ]),
            new Entity\IntegerField('CATEGORY_ID', [
                'required' => true,
            ]),
            new Entity\TextField('FIELDS', [
                'required' => true,
            ]),
            new Entity\StringField('VISIBILITY', [
                'required' => true,
                'validation' => function() {
                    return [
                        new Entity\Validator\Length(null, 255),
                    ];
                }
            ]),
            new Entity\StringField('GROUP_VISIBILITY', [
                'required' => true,
                'validation' => function() {
                    return [
                        new Entity\Validator\Length(null, 255),
                    ];
                }
            ]),
            new Entity\IntegerField('BUSINESS_PROCESS_ID', [
                'required' => true,
            ]),
            new Entity\IntegerField('PROCESS_ID', [
                'required' => true,
            ]),
        ];
    }
}
?>
