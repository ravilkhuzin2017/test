<?php
$arJsConfig = array(
    'unify-time-manage-form' => array(
        'js' => '/local/php_interface/unify-time-manage-form/app.js',
        'css' => '/local/php_interface/unify-time-manage-form/app.css',
        'rel' => array(),
    ),
    'jquery-unify' => array(
        'js' => '/local/php_interface/unify-time-manage-form/jquery.min.js',
    ),
);
foreach ($arJsConfig as $ext => $arExt) {
    \CJSCore::RegisterExt($ext, $arExt);
}

\CUtil::InitJSCore(array('jquery-unify'));
\CUtil::InitJSCore(array('unify-time-manage-form'));


