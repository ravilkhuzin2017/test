<?php


namespace Unify;

use Bitrix\{
    Main\Localization\Loc,
    Main\ORM\Fields\BooleanField,
    Main\ORM\Fields\DatetimeField,
    Main\ORM\Fields\IntegerField,
    Main\ORM\Fields\Relations\Reference,
    Main\ORM\Query\Join,
    Main\Type\DateTime
};

Loc::loadMessages(__FILE__);


class UnifyTables extends \Bitrix\Main\ORM\Data\DataManager
{
    public static $table_tasks = 'b_tasks';
    public static $table_tasks_elapsed_time = 'b_tasks_elapsed_time';

    public static function getTasks()
    {
        $connection = \Bitrix\Main\Application::getConnection();
        $addition_sql = '';
        if (!empty($_POST['filtering'])) {
            $from = !empty($_POST['from']) ? htmlspecialchars(trim($_POST['from'])) : false;
            $to = !empty($_POST['to']) ? htmlspecialchars(trim($_POST['to'])) : false;
            $group_id = !empty($_POST['group_id']) ? htmlspecialchars(trim($_POST['group_id'])) : false;
            $responsible_id = !empty($_POST['responsible_id']) ? htmlspecialchars(trim($_POST['responsible_id'])) : false;

            if ($from && !$to) {
                $from = date('Y-m-d H:i:s', strtotime($from));
                $addition_sql .= " AND be.CREATED_DATE >= '$from'";
            }
            if ($to && !$from) {
                $to = date('Y-m-d H:i:s', strtotime($to));
                $addition_sql .= " AND be.CREATED_DATE <= '$to'";
            } else if ($from && $to) {
                $from = date('Y-m-d H:i:s', strtotime($from));
                $to = date('Y-m-d H:i:s', strtotime($to));
                $addition_sql .= " AND be.CREATED_DATE BETWEEN '$from' AND '$to' ";
            }

            if ($group_id) {
                $addition_sql .= " AND bt.ID = $group_id ";
            }

            if ($responsible_id) {
                $addition_sql .= " AND bu.ID = $responsible_id ";
            }
        }

//        echo $addition_sql;

        $out = $connection->query("
        SELECT 
        bt.ID as TASK_ID,
        bt.GROUP_ID as GROUP_ID,
        bu.ID as RESPONSIBLE_ID,
        CONCAT(bu.NAME, ' ', bu.LAST_NAME) as RESPONSIBLE_NAME,
        bt.TITLE as TASK_TITLE,
        SUM(be.MINUTES) as MINUTES,
        GROUP_CONCAT(be.COMMENT_TEXT SEPARATOR '<br> ') as COMMENTS,
        GROUP_CONCAT(bl.NAME SEPARATOR ', ') as TAGS,
        be.CREATED_DATE as CREATED_DATE
        FROM b_tasks bt 
        LEFT JOIN b_tasks_elapsed_time be ON bt.ID = be.TASK_ID
        LEFT JOIN   b_tasks_label bl ON bt.GROUP_ID = bl.GROUP_ID
        LEFT JOIN b_user bu ON bu.ID = be.USER_ID
        WHERE bt.STATUS IN (2,3,4) $addition_sql GROUP BY bu.ID, bt.ID ORDER BY bt.CREATED_DATE DESC
        ");
        $arRes = $out->fetchAll();
        return $arRes;
    }

    public static function getTasksList()
    {
        $connection = \Bitrix\Main\Application::getConnection();
        $out = $connection->query("SELECT ID, TITLE, GROUP_ID  FROM b_tasks WHERE STATUS IN (2,3,4) ORDER BY CREATED_DATE DESC");
        $arRes = $out->fetchAll();
        return $arRes;
    }

    public static function getGroupList()
    {
        $connection = \Bitrix\Main\Application::getConnection();
        $out = $connection->query("SELECT ID, NAME  FROM b_sonet_group WHERE ACTIVE = 'Y' ORDER BY DATE_CREATE DESC");
        $arRes = $out->fetchAll();
        return $arRes;
    }

    public static function updateCreatedDate($date, $id)
    {
        $connection = \Bitrix\Main\Application::getConnection();
        $out = $connection->query("UPDATE b_tasks_elapsed_time SET CREATED_DATE = '$date' WHERE ID=$id");
        return $out;
    }

    public static function getUsersLIst()
    {
        $connection = \Bitrix\Main\Application::getConnection();
        $out = $connection->query("SELECT ID, NAME, LAST_NAME  FROM b_user");
        $arRes = $out->fetchAll();
        return $arRes;
    }

}