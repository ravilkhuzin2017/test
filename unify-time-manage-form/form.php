<?php
require_once($_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/main/include/prolog_before.php');
require_once 'class.php';


if(isset($_POST['unify_add_time']) == 'save') {
    if (CModule::IncludeModule("tasks")) {
        $arFields = array(
            "USER_ID" => $USER->getID(),
            "TASK_ID" => !empty($_POST['task_id']) ? $_POST['task_id'] : '',
            "MINUTES" => !empty($_POST['hours']) ? ($_POST['hours']*60) + $_POST['minutes'] : '',
            "COMMENT_TEXT" => !empty($_POST['comment']) ? $_POST['comment']: '',
        );
        $obElapsed = new CTaskElapsedTime();
        $out = $obElapsed->Add($arFields);

        if($out) {
            \Unify\UnifyTables::updateCreatedDate($_POST['date'], $out);
        }

    }
}
?>
<script src="js/jquery.min.js"></script>
<script src="js/app.js?v=<?= time()?>"></script>
<link rel="stylesheet" type="text/css" href="css/app.css?v=<?= time()?>">

<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/flatpickr/dist/flatpickr.min.css">
<script src="https://cdn.jsdelivr.net/npm/flatpickr"></script>
<script src="https://npmcdn.com/flatpickr/dist/l10n/ru.js"></script>




<div class="two"><h1>Записать часы</h1></div>
<form action="" method="post">
<section class="pttime-processing-content">
    <div class="time-wrapper" style="padding: 20px;">
        <div class="left">
                <label>Дата и время</label>
                <input type="text" name="date" value="" required autocomplete="off" class="datepicker">


                <br>
                <br>
                <label>Затрачено:</label>
                <input type="text" name="hours" required placeholder="Часов" value=""  style="background: white; width: 100px; margin-right: 10px;">
                <input type="text" name="minutes" required placeholder="Минут"  value=""  style="background: white; width: 100px;">

                <br>
                <br>
                <label>Комментарий:</label>
                <textarea name="comment" style="height: 250px;"></textarea>
                <br>
                <button type="submit" value="save" name="unify_add_time" class="my-button">Сохранить</button>

        </div>
        <div class="right">

            <br><label>Отфильтровать задачи по проекту:</label>
        <select name="group_id" class="js-change-group-id" required>
            <option value="all">Все проекты</option>
            <?php foreach (\Unify\UnifyTables::getGroupList() as $item):?>
                <option value="<?= $item['ID']?>"><?= $item['NAME']?></option>
            <?php endforeach;?>
        </select>
            <br>
            <br><label>Выберите задачу:</label>
        <select name="task_id" required multiple="multiple" style="width: 100%; height: 500px; background: white">
            <?php foreach (\Unify\UnifyTables::getTasksList() as $item):?>
                <option value="<?= $item['ID']?>" data-gr="<?= $item['GROUP_ID']?>"><?= $item['TITLE']?></option>
            <?php endforeach;?>
        </select>
        </div>
    </div>
</section>
</form>


<script>
    $(".datepicker").flatpickr({
        enableTime: true,
        dateFormat: "Y-m-d H:i:s",
        locale: "ru"
    });
    window.addEventListener('beforeunload', function(event) {
        window.location.href = window.location.origin
    });
</script>