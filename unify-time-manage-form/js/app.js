$ = jQuery;
function JsPttimeUnify(config) {
    this.config = config || {};
    this.init();
}

JsPttimeUnify.prototype = {
    init: function () {
        console.log('init Micros: ' + this.config.name);
        var self = this;
        $(document).on('click touch', '.js-open-time-form', function (e){
            self.openSideBar($(this));
        });
        $(document).on('click touch', '.js-change-group-id', function (e){
            self.filterByGroup($(this));
        });

        window.addEventListener('beforeunload', function(event) {
            BX.SidePanel.Instance.close([immediately=false]);
        });

        var buttonTest = `
        <div class="timeman-container timeman-container-ru" id="timeman-container" style="display: flex; align-content: center; flex-direction: row; align-items: center;justify-content: center;">
            <div class="timeman-wrap">
            <button class="ui-btn ui-btn-primary js-open-time-form">Записать часы</button>
            </div>
        </div>
        `;

        if($('.js-open-time-form').length == 0 ) {
            $('.timeman-container').before(buttonTest);
        }
    },
    filterByGroup: function ($self){
        var $value = $self.val();
        $('[data-gr]').css({display: 'none'})
        if($value == 'all') {
            $('[data-gr]').css({display: 'block'})
        } else {
            $('[data-gr="' + $value + '"]').css({display: 'block'});
        }
    },
    openSideBar: function ($self){
        BX.SidePanel.Instance.open("/local/php_interface/unify-time-manage-form/form.php", {
            requestMethod: "post",
        });
    }
}

$(document).ready(function() {
    new JsPttimeUnify({
        name: 'Pttime Unify'
    });
});
